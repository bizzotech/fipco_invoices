var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var flash = require('connect-flash-light');
var SHA256 = require("crypto-js/sha256");
var Sequelize = require('sequelize');
var lusca = require('lusca');
require('sequelize-virtual-fields')(Sequelize);
var sequelize;
if (process.env.DATABASE_URL) {
  sequelize = new Sequelize(process.env.DATABASE_URL);
} else {
  if (process.env.NODE_ENV == "test") {
    sequelize = new Sequelize('database', null, null, {
      host: 'localhost',
      dialect: 'sqlite',
      logging: false,
      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
      storage: 'test.sqlite'
    });
  } else {
    sequelize = new Sequelize('database', null, null, {
      host: 'localhost',
      dialect: 'sqlite',

      pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
      storage: 'database.sqlite'
    });
  }

}

var models = {};
require('./server/models')(models, Sequelize, sequelize);
var routes = require('./server/routes');





var app = express();
app.use(express.static('public'));
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true
}));
app.use(cookieParser());
app.use(session({
  secret: 'AA',
  cookie: {
    httpOnly: true,
  }
}));
app.use(flash());
app.disable('x-powered-by');
app.use(lusca.csp({
  policy: {
    'default-src': '\'self\' *.googleapis.com *.gstatic.com malsup.github.io',
    'img-src': '*',
    'style-src': "'self' 'unsafe-inline' *.googleapis.com"
  }
}));
var ensureAuthenticated = require('./server/auth')(app, models, sequelize);




sequelize.sync().done(function() {
  models.Users.findAll().done(function(users) {

    if (users.length === 0) {
      var new_user = {
        name: "Admin",
        username: "admin",
        password: SHA256("admin" + "BizzoSecret").toString(),
        phone: "000",
        role: "admin"
      };
      models.Users.create(new_user).then(function() {

      });
    }
  });
});
sequelize.initVirtualFields();

require('marko/node-require').install();
var layout_template = require('./layout.marko');
var meta = require('./meta');
app.get('/', ensureAuthenticated, function(req, res) {
  //res.sendFile(__dirname + "/" + "app.html");
  var user = '%-' + req.user.id + '-%';

  models.Payment.findAll({
    attributes: ['invoice_id'],
    where: ['"payment"."to_notify" LIKE ?', user],
    group: ['invoice_id']
  }).then(function(payments) {
    console.log("#####################");
    console.log(JSON.stringify(payments));
    layout_template.render({
      user: req.user,
      new_invoices_count: payments.length,
      css : process.env.NODE_ENV != "development" ? ['/lib/css/app.min.css'] : meta.css,
      js : process.env.NODE_ENV != "development" ? ['/lib/js/app.min.js'] : meta.js,
      client_file: process.env.PORT ? "app.min.js" : "app.js"
    }, res);
  });

});

routes(app, models, ensureAuthenticated, Sequelize, sequelize);

var server = app.listen(process.env.NODE_ENV == "test" ? "8081" : (process.env.PORT || 8080), function() {

  var host = server.address().address;
  var port = server.address().port;

  console.log("Example app listening at http://%s:%s", host, port);

});

module.exports = {
  models : models
};

var moment = require('moment');
var R = require('ramda');

module.exports = function(models, Sequelize, sequelize) {



  models.Customer = sequelize.define('customer', {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    code: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    area: {
      type: Sequelize.STRING
    },
    customer_segment_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    }
  }, {
    freezeTableName: true
  });

  models.Invoice = sequelize.define('invoice', {
      customer_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      user_id: {
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      serial: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      }
    },

    {
      freezeTableName: true,
      instanceMethods: {
        calculateAggragetedPayments: function() {
          var payments = this.dataValues.payments.filter(function(payment) {
            return !payment.non_commissionable;
          });
          var sumAmounts = R.compose(R.sum, R.pluck('amount'));

          function latencyBetween(start, end) {
            return R.both(R.propSatisfies(R.gte(R.__, start), 'latency'),
              R.propSatisfies(R.lt(R.__, end), 'latency'));
          }


          var lastPaymentLatency = Math.max.apply({}, R.pluck('latency', payments));
          var multipleOf30 = R.compose(R.equals(0), R.modulo(R.__, 30));
          var periods = R.filter(multipleOf30, R.range(0, lastPaymentLatency + 1));

          var aggregated_payments = R.map(function(p) {
            return sumAmounts(R.filter(latencyBetween(p, p + 30), payments));
          }, periods);

          this.dataValues.aggregated_payments = aggregated_payments;
          return this;
        },
        calculateAggragetedPaymentsPerLine: function() {
          var aggregated_payments = this.dataValues.aggregated_payments;
          var invoice_lines = this.dataValues.invoice_lines;
          var total_lines = R.sum(R.pluck('sub_total', invoice_lines));
          invoice_lines.forEach(function(line) {
            //line.paid = (line.sub_total / total_lines) * sum_of_payments;
            aggregated_payments_per_line = [];
            aggregated_payments.forEach(function(p) {
              aggregated_payments_per_line.push((line.sub_total / total_lines) * p);
              line.dataValues.aggregated_payments_per_line = aggregated_payments_per_line;
            });
          });
          return this;
        },
        calculateCommissions: function() {
          var invoice = this.calculateAggragetedPayments().calculateAggragetedPaymentsPerLine();
          var rules = invoice.dataValues.customer.dataValues.customer_segment.dataValues.commission_rules;
          console.log(rules);
          invoice.dataValues.commissions_per_rule = {};
          invoice.dataValues.invoice_lines.forEach(function(line) {
            var line_rules = rules.filter(function(rule) {
              return rule.dataValues.product_category_id == line.dataValues.product.dataValues.product_category_id;
            });
            console.log(line_rules);
            line.dataValues.commission = 0;
            if (line.dataValues.aggregated_payments_per_line) {
              line_rules.forEach(function(rule) {
                var start = rule.dataValues.latency_from / 30;
                var end = rule.dataValues.latency_to / 30;
                var to_be_commissioned = R.sum(line.dataValues.aggregated_payments_per_line.slice(start, end));
                console.log(to_be_commissioned);
                var commission = 0;
                if (rule.dataValues.percentage_or_amount == "percentage") {
                  commission += to_be_commissioned * rule.dataValues.commission_base * rule.dataValues.commission_value;
                } else {
                  commission += to_be_commissioned * line.dataValues.sub_total * line.dataValues.quantity * rule.dataValues.commission_value;
                }
                line.dataValues.commission += commission;
                invoice.dataValues.commissions_per_rule[rule.id] = {
                  value: commission + (invoice.dataValues.commissions_per_rule[rule.id] || 0),
                  rule: rule
                };
              });
            }

          });
          return this;
        }
      }
    }



  );

  models.Payment = sequelize.define('payment', {
      invoice_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      amount: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      cash_or_credit: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      deposit_number: {
        type: Sequelize.STRING
      },
      notes: {
        type: Sequelize.STRING
      },
      state: {
        type: Sequelize.STRING
      },
      received: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      to_notify: {
        type: Sequelize.STRING
      },
      latency: {
        type: Sequelize.VIRTUAL,
        get: function() {
          if (this.due_date && this.due_date != "Invalid Date") {
            //console.error("########## DUE DATE ##############");
            //console.error(this.due_date);
            if (moment(this.date).isAfter(this.due_date)) {
              return moment(this.date).diff(moment(this.invoice.date), 'days');
            } else {
              return moment(this.due_date).diff(moment(this.invoice.date), 'days');
            }
          }

          return moment(this.date).diff(moment(this.invoice.date), 'days');
        },
        attributes: ['date', 'due_date'],
        include: [{
          model: models.Invoice,
          attributes: ['date']
        }],
      },
      cheque_number: {
        type: Sequelize.STRING
      },
      due_date: {
        type: Sequelize.DATE
      },
      bank_name: {
        type: Sequelize.STRING
      },
      non_commissionable: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      }
    },

    {
      freezeTableName: true
    });

  models.Users = sequelize.define('users', {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    phone: {
      type: Sequelize.STRING,
    },
    role: {
      type: Sequelize.STRING,
      allowNull: false,
    }
  }, {
    freezeTableName: true
  });

  models.ProductCategory = sequelize.define('product_category', {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    }
  }, {
    freezeTableName: true
  });

  models.Product = sequelize.define('product', {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    code: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    },
    product_category_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    }
  }, {
    freezeTableName: true
  });

  models.CustomerSegment = sequelize.define('customer_segment', {
    name: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true
    }
  }, {
    freezeTableName: true
  });

  models.InvoiceLine = sequelize.define('invoice_line', {
    invoice_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    product_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    quantity: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    price: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    sub_total: {
      type: Sequelize.VIRTUAL,
      get: function() {
        return this.quantity * this.price;
      },
      attributes: ['quantity', 'price'],
    }
  }, {
    freezeTableName: true
  });

  models.CommissionRule = sequelize.define('commission_rule', {
    latency_from: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique : "unique_rule",
    },
    latency_to: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique : "unique_rule",
    },
    customer_segment_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique : "unique_rule",
    },
    product_category_id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      unique : "unique_rule",
    },
    commission_base: {
      type: Sequelize.FLOAT,
      allowNull: false,
    },
    percentage_or_amount: {
      type: Sequelize.STRING,
      allowNull: false,
      unique : "unique_rule",
    },
    commission_value: {
      type: Sequelize.FLOAT,
      allowNull: false,
    }
  }, {
    defaultScope: {
      include: [{
        model: models.CustomerSegment
      }, {
        model: models.ProductCategory
      }]
    },
    freezeTableName: true
  });


  models.Customer.hasMany(models.Invoice, {
    foreignKey: 'customer_id',
  });
  models.Invoice.belongsTo(models.Customer, {
    foreignKey: 'customer_id',
  });


  models.Invoice.hasMany(models.Payment, {
    foreignKey: 'invoice_id',
  });
  models.Payment.belongsTo(models.Invoice, {
    foreignKey: 'invoice_id',
  });

  models.Users.hasMany(models.Invoice, {
    foreignKey: 'user_id',
  });
  models.Invoice.belongsTo(models.Users, {
    foreignKey: 'user_id',
  });

  models.CustomerSegment.hasMany(models.Customer, {
    foreignKey: 'customer_segment_id',
  });

  models.Customer.belongsTo(models.CustomerSegment, {
    foreignKey: 'customer_segment_id',
  });

  models.Invoice.hasMany(models.InvoiceLine, {
    foreignKey: 'invoice_id',
  });

  models.InvoiceLine.belongsTo(models.Invoice, {
    foreignKey: 'invoice_id',
  });

  models.Product.hasMany(models.InvoiceLine, {
    foreignKey: 'product_id',
  });

  models.InvoiceLine.belongsTo(models.Product, {
    foreignKey: 'product_id',
  });

  models.ProductCategory.hasMany(models.Product, {
    foreignKey: 'product_category_id',
  });

  models.Product.belongsTo(models.ProductCategory, {
    foreignKey: 'product_category_id',
  });


  models.CommissionRule.belongsTo(models.CustomerSegment, {
    foreignKey: 'customer_segment_id',
  });

  models.CustomerSegment.hasMany(models.CommissionRule, {
    foreignKey: 'customer_segment_id',
  });


  models.CommissionRule.belongsTo(models.ProductCategory, {
    foreignKey: 'product_category_id',
  });

  models.ProductCategory.hasMany(models.CommissionRule, {
    foreignKey: 'product_category_id',
  });

};

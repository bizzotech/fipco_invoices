var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  app.get('/customers-table', ensureAuthenticated, function(req, res, next) {
    req.Model = models.Customer;
    req.search_columns = ['"customer_segment"."name"', '"customer"."name"', '"customer"."code"'];
    req.include = [{
      model: models.CustomerSegment
    }];
    next();
  }, utils.handleList);

  app.post('/import/customers', upload.single('customers'), function(req, res, next) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    xls(req.file.path, function(err, data) {
      if (err) throw err;
      // data is an array of arrays
      //console.log(data);
      var customers = utils.convertToJSON(data);
      customers = R.uniqBy(function(customer) {
        return customer.code;
      }, customers);
      models.CustomerSegment.findAll().then(function(customer_segments) {
        customers.forEach(function(customer) {
          for (var key in customer) {
            if (key == 'customer_segment.name') {
              customer.customer_segment_id = R.find(R.propEq('name', customer[key]))(customer_segments).id;
            }
          }
        });
        models.Customer.bulkCreate(customers).then(function() {
          res.send(200);
        });

      });

      console.log(req.file.path);

    });

  });

  app.get('/customers/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Customer.findById(id, {
      include: [{
        model: models.CustomerSegment
      }]
    }).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/customers', ensureAuthenticated, function(req, res) {
    models.Customer.findAll({
      include: [{
        model: models.CustomerSegment
      }]
    }).then(responseOr404(res)).catch(error500(res));
  });

  app.post('/customers', ensureAuthenticated, function(req, res) {
    models.Customer.create(req.body).then(function() {
      res.send(201);
    }).catch(error500(res));
  });

  app.put('/customers/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Customer.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      res.send(200);
    }).catch(error500(res));
  });


};

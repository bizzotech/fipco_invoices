var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  app.get('/commission-rules-table', ensureAuthenticated, function(req, res, next) {
    req.Model = models.CommissionRule;
    req.search_columns = ['"customer_segment"."name"', '"product_category"."name"'];
    req.include = [{
      model: models.CustomerSegment
    }, {
      model: models.ProductCategory
    }];
    next();
  }, utils.handleList);

  //commission rules

  app.get('/commission-rules/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.CommissionRule.findById(id).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/commission-rules', ensureAuthenticated, function(req, res) {
    models.CommissionRule.findAll().then(responseOr404(res)).catch(error500(res));
  });

  app.post('/commission-rules', ensureAuthenticated, function(req, res) {
    models.CommissionRule.create(req.body).then(function() {
      res.send(201);
    }).catch(error500(res));
  });

  app.put('/commission-rules/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.CommissionRule.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      res.send(200);
    }).catch(error500(res));
  });
};

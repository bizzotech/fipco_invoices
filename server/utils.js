var R = require('ramda');


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


var utils = {
  randomString: function randomString(len) {
    var buf = [];
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charlen = chars.length;

    for (var i = 0; i < len; ++i) {
      buf.push(chars[getRandomInt(0, charlen - 1)]);
    }

    return buf.join('');
  },
  convertToJSON: function convertToJSON(array) {
    var first = array[0].join();
    var headers = first.split(',');

    var jsonData = [];
    for (var i = 1, length = array.length; i < length; i++) {

      var myRow = array[i].join();
      var row = myRow.split(',');

      var data = {};
      for (var x = 0; x < row.length; x++) {
        data[headers[x]] = row[x];
      }
      jsonData.push(data);

    }
    return jsonData;
  },
  responseOr404 : R.curry(function responseOr404(res, result) {
    if (result !== null) {
      //console.log(result);
      res.json(result);
    } else {
      res.send(404);
    }
  }),
  error500 : R.curry(function error500(res, err) {
    console.log('Error occured', err);
    res.status(500).json(err);
  }),
  handleList : function handleList(req, res){
    var Model = req.Model;//models.CustomerSegment;
    var search_columns = req.search_columns;//['"customer_segment"."name"'];
    var include = req.include;//[];

    var search_value = req.query.search.value;
    var where = [];
    if (search_value) {
      var query = '%' + search_value + '%';
      var search_columns_like = search_columns.map(function(col) {
        return col + " LIKE ?";
      });
      var search_columns_like_str = search_columns_like.join(" OR ");
      where.push(search_columns_like_str);
      search_columns.forEach(function() {
        where.push(query);
      });
      //where = ['"customer_segment"."name" LIKE ?', query];
    }

    var order_by = req.query.order[0].column;
    var order_dir = req.query.order[0].dir;

    Model.count({
      where: where,
      include: include
    }).then(function(objects_count) {
      Model.findAll({
        where: where,
        include: include,
        limit: req.query.length,
        offset: req.query.start,
        order: [
          [order_by, order_dir]
        ],
        subQuery: false
      }).then(function(objects) {
        return {
          draw: req.query.draw,
          recordsTotal: objects_count,
          recordsFiltered: objects_count,
          data: objects
        };
      }).then(utils.responseOr404(res)).catch(utils.error500(res));
    });

  }
};

module.exports = utils;

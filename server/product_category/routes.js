var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  app.get('/product-categories-table', ensureAuthenticated, function(req, res, next) {
    req.Model = models.ProductCategory;
    req.search_columns = ['"product_category"."name"'];
    req.include = [];
    next();
  }, utils.handleList);

  // ProductCategories Routes
  app.get('/product-categories/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.ProductCategory.findById(id).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/product-categories', ensureAuthenticated, function(req, res) {
    models.ProductCategory.findAll().then(responseOr404(res)).catch(error500(res));
  });

  app.post('/product-categories', ensureAuthenticated, function(req, res) {
    models.ProductCategory.create(req.body).then(function() {
      res.send(201);
    }).catch(error500(res));
  });

  app.put('/product-categories/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.ProductCategory.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      res.send(200);
    }).catch(error500(res));
  });

};

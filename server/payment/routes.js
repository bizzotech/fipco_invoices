var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  //payments routes
  app.get('/payments/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Payment.findById(id).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/payments', ensureAuthenticated, function(req, res) {
    models.Payment.findAll().then(responseOr404(res)).catch(error500(res));
  });

  app.post('/payments', ensureAuthenticated, function(req, res) {

    models.Users.findAll({
      where: {
        role: "accountant",
        id: {
          $ne: req.user.id
        }
      }
    }).done(function(users) {
      console.log("####### USERS #######");
      console.log(users);
      var to_notify = "-" + users.map(function(x) {
        return x.id;
      }).join("-") + "-";
      req.body.to_notify = to_notify;
      models.Payment.create(req.body).then(function() {
        models.Invoice.update({}, {
          where: {
            id: req.body.invoice_id
          }
        });
        res.send(201);
      }).catch(error500(res));
    });
  });

  app.get('/invoice/:id/payments', ensureAuthenticated, function(req, res) {
    var id = Number(req.params.id);
    models.Payment.findAll({
      where: {
        invoice_id: id
      }
    }).then(responseOr404(res)).catch(error500(res));
  });

  app.put('/payments/:id', ensureAuthenticated, function(req, res) {

    models.Users.findAll({
      where: {
        role: "accountant",
        id: {
          $ne: req.user.id
        }
      }
    }).done(function(users) {
      console.log("####### USERS #######");
      console.log(users);
      var to_notify = "-" + users.map(function(x) {
        return x.id;
      }).join("-") + "-";
      var id = req.params.id;
      req.body.to_notify = to_notify;
      models.Payment.update(req.body, {
        where: {
          id: id
        }
      }).then(function(result) {
        res.json(result);
      }).catch(error500(res));
    });
  });

};

var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  app.get('/customer-segments-table', ensureAuthenticated, function(req, res, next) {
    req.Model = models.CustomerSegment;
    req.search_columns = ['"customer_segment"."name"'];
    req.include = [];
    next();
  }, utils.handleList);

  // Customer Segment Routes
  app.get('/customer-segments/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.CustomerSegment.findById(id).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/customer-segments', ensureAuthenticated, function(req, res) {
    models.CustomerSegment.findAll().then(responseOr404(res)).catch(error500(res));
  });

  app.post('/customer-segments', ensureAuthenticated, function(req, res) {
    models.CustomerSegment.create(req.body).then(function() {
      res.send(201);
    }).catch(error500(res));
  });

  app.put('/customer-segments/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.CustomerSegment.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      res.send(200);
    }).catch(error500(res));
  });

};

var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  app.get('/invoices-table', ensureAuthenticated, function(req, res, next) {
    var user_where = [];
    if (req.query.agent_invoices) {
      user_where = [{
        id: req.user.id
      }];
    }

    req.Model = models.Invoice;
    req.search_columns = ['"invoice"."serial"', '"customer"."name"', '"customer"."code"'];
    req.include = [{
      model: models.Customer,
    }, {
    //   model: models.Payment
    // }, {
    //   model: models.InvoiceLine
    // }, {
      model: models.Users,
      where: user_where
    }];
    next();
  }, utils.handleList);

  //invoices routes
  app.get('/invoices/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Invoice.findById(id, {
      include: [{
        model: models.Customer,
        include: [{
          model: models.CustomerSegment,
          include: [{
            model: models.CommissionRule
          }]
        }]
      }, {
        model: models.Users
      }, {
        model: models.Payment
      }, {
        model: models.InvoiceLine,
        include: [{
          model: models.Product
        }]
      }]
    }).then(function(invoice) {
      var payments = invoice.dataValues.payments;
      var invoice_lines = invoice.dataValues.invoice_lines;

      var sum_of_payments = R.sum(R.pluck('amount', payments));
      var total_lines = R.sum(R.pluck('sub_total', invoice_lines));

      invoice.dataValues.sum_of_payments = sum_of_payments;
      invoice.dataValues.total_lines = total_lines;

      invoice_lines.forEach(function(line) {
        line.paid = (line.sub_total / total_lines) * sum_of_payments;
      });
      return invoice;
    }).then(function(invoice) {
      return invoice.calculateCommissions();
    }).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/invoices', ensureAuthenticated, function(req, res) {
    models.Invoice.findAll({
      include: [{
        model: models.Customer
      }, {
        model: models.Payment
      }, {
        model: models.InvoiceLine
      }]
    }).then(responseOr404(res)).catch(error500(res));
  });

  app.post('/invoices', ensureAuthenticated, function(req, res) {
    models.Invoice.create(req.body).then(function(invoice) {
      if (req.body.invoice_lines) {
        req.body.invoice_lines.forEach(function(line) {
          line.invoice_id = invoice.id;
          models.InvoiceLine.create(line);
        });
      }
      res.send(201);
    }).catch(error500(res));
  });

  app.put('/invoices/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Invoice.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      if (req.body.invoice_lines) {
        req.body.invoice_lines.forEach(function(line) {
          line.invoice_id = id;
          if (line.id && line.edited) {
            models.InvoiceLine.update(line, {
              where: {
                id: line.id
              }
            });
          } else {
            models.InvoiceLine.create(line);
          }
        });
      }
      if (req.body.payments) {
        req.body.payments.forEach(function(payment) {
          payment.invoice_id = id;
          models.Users.findAll({
            where: {
              role: "accountant",
              id: {
                $ne: req.user.id
              }
            }
          }).then(function(users) {
            //console.log("####### USERS #######");
            //console.log(users);
            var to_notify = "-" + users.map(function(x) {
              return x.id;
            }).join("-") + "-";
            payment.to_notify = to_notify;
            if (payment.id && payment.edited) {
              models.Payment.update(payment, {
                where: {
                  id: payment.id
                }
              });
            } else {
              models.Payment.create(payment);
            }
          });

        });
      }

      res.send(200);
    }).catch(error500(res));
  });

  // InvoiceLine Routes
  app.get('/invoice-lines/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.InvoiceLine.findById(id).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/invoice-lines', ensureAuthenticated, function(req, res) {
    models.InvoiceLine.findAll().then(responseOr404(res)).catch(error500(res));
  });

  app.post('/invoice-lines', ensureAuthenticated, function(req, res) {
    models.InvoiceLine.create(req.body).then(function() {
      res.send(201);
    }).catch(error500(res));
  });

  app.put('/invoice-lines/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.InvoiceLine.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      res.send(200);
    }).catch(error500(res));
  });

  app.get('/invoice/notify', ensureAuthenticated, function(req, res) {
    var user = '%-' + req.user.id + '-%';
    models.Payment.findAll({
      where: ['"payment"."to_notify" LIKE ?', user]
    }).then(function(payments) {
      var count = [];
      payments.forEach(function(payment) {
        var index = count.indexOf(payment.invoice_id);
        if (index == -1) {
          count.push(payment.invoice_id);
        }
      });
      res.json(count);
    }).catch(error500(res));
  });


};

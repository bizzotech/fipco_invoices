var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  app.get('/products-table', ensureAuthenticated, function(req, res) {
    var search_value = req.query.search.value;
    var where = [];
    if (search_value) {
      var query = '%' + search_value + '%';
      where = ['"product_category"."name" LIKE ? OR "product"."name" LIKE ? OR "product"."code" LIKE ?', query, query, query];
    }
    var order_by = req.query.order[0].column;
    //var order_by = req.query.columns[order_by_index].name;
    var order_dir = req.query.order[0].dir;

    models.Product.count({
      where: where,
      include: [{
        model: models.ProductCategory
      }]
    }).then(function(products_count) {
      models.Product.findAll({
        where: where,
        include: [{
          model: models.ProductCategory
        }],
        limit: req.query.length,
        offset: req.query.start,
        order: [
          [order_by, order_dir]
        ],
      }).then(function(products) {
        return {
          draw: req.query.draw,
          recordsTotal: products_count,
          recordsFiltered: products_count,
          data: products
        };
      }).then(responseOr404(res)).catch(error500(res));
    });

  });

  // Product Routes
  app.get('/products/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Product.findById(id, {
      include: [{
        model: models.ProductCategory
      }]
    }).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/products', ensureAuthenticated, function(req, res) {
    models.Product.findAll({
      include: [{
        model: models.ProductCategory
      }]
    }).then(responseOr404(res)).catch(error500(res));
  });

  app.post('/products', ensureAuthenticated, function(req, res) {
    models.Product.create(req.body).then(function() {
      res.send(201);
    }).catch(error500(res));
  });

  app.put('/products/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Product.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      res.send(200);
    }).catch(error500(res));
  });


};

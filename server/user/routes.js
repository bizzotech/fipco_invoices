var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});

var xls = require('excel');
var utils = require('../utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  app.get('/users-table', ensureAuthenticated, function(req, res, next) {
    req.Model = models.Users;
    req.search_columns = ['"users"."name"', '"users"."username"', '"users"."role"', '"users"."phone"'];
    req.include = [];
    next();
  }, utils.handleList);

  //users routes
  app.post('/users', ensureAuthenticated, function(req, res) {

    var new_user = {
      name: req.body.name,
      username: req.body.username,
      password: SHA256(req.body.password + "BizzoSecret").toString(),
      phone: req.body.phone,
      role: req.body.role
    };
    models.Users.create(new_user).then(function() {
      res.send(201);
    }).catch(error500(res));
  });

  app.get('/users/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;

    models.Users.findById(id).then(responseOr404(res)).catch(error500(res));
  });

  app.get('/users', ensureAuthenticated, function(req, res) {
    models.Users.findAll().then(responseOr404(res)).catch(error500(res));
  });

  app.put('/users/:id', ensureAuthenticated, function(req, res) {
    //<<<<<<< HEAD

    //=======
    console.log("Body of Req " + JSON.stringify(req.body));
    //>>>>>>> c960a06ebdf858842b786ea117ff5baae223027c
    var id = req.params.id;
    models.Users.update(req.body, {
      where: {
        id: id
      }
    }).then(function() {
      res.send('this is an update');
    }).catch(error500(res));
  });

};

var SHA256 = require("crypto-js/sha256");
var R = require('ramda');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/'
});
var utils = require('./utils');

var responseOr404 = utils.responseOr404;
var error500 = utils.error500;

module.exports = function(app, models, ensureAuthenticated, Sequelize, sequelize) {
  require('./customer/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);
  require('./customer_segment/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);
  require('./invoice/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);
  require('./product/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);
  require('./product_category/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);
  require('./payment/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);
  require('./user/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);
  require('./commission_rule/routes')(app, models, ensureAuthenticated, Sequelize, sequelize);

  app.get('/customer/:id/invoices', ensureAuthenticated, function(req, res) {
    var id = Number(req.params.id);
    models.Invoice.findAll({
      where: {
        customer_id: id
      }
    }).then(responseOr404(res)).catch(error500(res));
  });


  app.get('/search/invoices/:query', ensureAuthenticated, function(req, res) {
    if (req.params.query == 'ALL') {
      models.Invoice.findAll({
        include: [{
          model: models.Customer
        }, {
          model: models.Users
        }]
      }).then(function(invoices) {
        res.json(invoices);
      });
    } else {
      var query = '%' + req.params.query + '%';
      models.Invoice.findAll({
        where: ['"invoice"."serial" LIKE ? OR "customer"."name" LIKE ? OR "customer"."code" LIKE ?', query, query, query],
        include: [{
          model: models.Customer
        }, {
          model: models.Users
        }]
      }).then(function(invoices) {
        res.json(invoices);
      });
    }

  });


  app.get('/user/current', ensureAuthenticated, function(req, res) {
    if (req.user) {
      res.json(req.user);
    }
  });

  app.get('/seen/invoices/:id', ensureAuthenticated, function(req, res) {
    var id = req.params.id;
    models.Invoice.findById(id).then(function(invoice) {
      if (invoice) {
        models.Payment.findAll({
          where: {
            invoice_id: id
          }
        }).then(function(payments) {
          payments.forEach(function(payment) {
            var to_be_notified = payment.to_notify.split('-');
            if (to_be_notified.indexOf(String(req.user.id)) != -1) {
              var to_be_notified_update = to_be_notified.filter(function(u_id) {
                return u_id && u_id != req.user.id;
              });
              var to_notify_update = "-" + to_be_notified_update.join("-") + "-";
              models.Payment.update({
                to_notify: to_notify_update
              }, {
                where: {
                  id: payment.id
                }
              });
            }
          });
          res.send(200);
        });
      } else {
        res.send(404);
      }
    }).catch(error500(res));
  });

};

var utils = require('./utils');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var RememberMeStrategy = require('passport-remember-me').Strategy;
var Sequelize = require('sequelize');
var SHA256 = require("crypto-js/sha256");


module.exports = function(app, models, sequelize) {
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(passport.authenticate('remember-me'));

  var Token = sequelize.define('token', {
    uid: {
      type: Sequelize.INTEGER,
    },
    token: {
      type: Sequelize.STRING
    }
  }, {
    freezeTableName: true
  });

  var tokens = {};

  function consumeRememberMeToken(token, fn) {
    Token.findOne({
      where: {
        token: token
      }
    }).then(function(t) {
      if (t) {
        fn(null, t.uid);
        t.destroy();
      } else {
        fn(null, false);
      }
      // Token.destroy({
      //   where : {
      //     uid : t.uid
      //   }
      // });
    });
  }

  function saveRememberMeToken(token, uid, fn) {
    Token.create({
      uid: uid,
      token: token
    }).then(function() {
      fn();
    });
  }


  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {

    models.Users.findById(id).then(function(user) {
      done(null, user);
    }).error(function(err) {
      done(err, false);
    });
  });


  passport.use(new LocalStrategy(
    function(username, password, done) {

      // Find the user by username.  If there is no user with the given
      // username, or the password is not correct, set the user to `false` to
      // indicate failure and set a flash message.  Otherwise, return the
      // authenticated `user`.
      models.Users.findOne({
        where: {
          username: username
        }
      }).then(function(user) {
        if (!user) {
          return done(null, false, {
            message: 'Unknown user ' + username
          });
        }
        if (user.password != SHA256(password + "BizzoSecret").toString()) {
          return done(null, false, {
            message: 'Invalid password'
          });
        }
        return done(null, user);
      }).error(function(err) {
        return done(err);
      });

    }
  ));

  function issueToken(user, done) {
    var token = utils.randomString(64);
    saveRememberMeToken(token, user.id, function(err) {
      if (err) {
        return done(err);
      }
      return done(null, token);
    });
  }
  passport.use(new RememberMeStrategy(
    function(token, done) {
      consumeRememberMeToken(token, function(err, uid) {
        if (err) {
          return done(err);
        }
        if (!uid) {
          return done(null, false);
        }

        models.Users.findById(uid).then(function(user) {
          if (!user) {
            return done(null, false);
          }
          return done(null, user);
        }).error(function(err) {
          return done(err);
        });
      });
    },
    issueToken
  ));


  app.get('/login', function(req, res) {
    res.sendFile(__dirname + "/" + "login.html");
  });

  // POST /login
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  If authentication fails, the user will be redirected back to the
  //   login page.  Otherwise, the primary route function function will be called,
  //   which, in this example, will redirect the user to the home page.
  //
  //   curl -v -d "username=bob&password=secret" http://127.0.0.1:3000/login
  app.post('/login',
    passport.authenticate('local', {
      failureRedirect: '/login',
      failureFlash: true
    }),
    function(req, res, next) {
      // Issue a remember me cookie if the option was checked
      if (!req.body.remember_me) {
        return next();
      }

      issueToken(req.user, function(err, token) {
        if (err) {
          return next(err);
        }
        res.cookie('remember_me', token, {
          path: '/',
          httpOnly: true,
          maxAge: 604800000
        });
        return next();
      });
    },
    function(req, res) {
      res.redirect('/');
    });

  app.get('/logout', function(req, res) {
    // clear the remember me cookie when logging out
    res.clearCookie('remember_me');
    req.logout();
    res.redirect('/');
  });

  return function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    console.log(req.route.path);
    if(req.route.path === "/"){
      res.redirect('/login');
    }else{
      res.sendStatus(401);
    }
  };
};

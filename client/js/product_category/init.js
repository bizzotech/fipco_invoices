module.exports = function(app) {
  app.product_category = {};
  app.product_category.models = {};
  app.product_category.view_models = {};
  app.product_category.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

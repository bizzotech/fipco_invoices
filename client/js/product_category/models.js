var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var validations = [{
    fn: function(category) {
      return !category.name;
    },
    msg: "من فضلك ادخل اسم فئة  المنتج"
  }];
  app.product_category.models.create = utils.create('/product-categories', validations);
  app.product_category.models.update = utils.update('/product-categories/', validations);
  app.product_category.models.fetch = utils.fetch('/product-categories/', R.identity);
  app.product_category.models.fetchList = function(data) {
    return $.get('/product-categories-table', data);
  };
};

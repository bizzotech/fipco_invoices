var ee = require('event-emitter');
var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  function buildProductCategoryVM(product_category, render) {
    var fields = {};
    fields.name = {
      value: product_category.name || ""
    };
    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      object_id: product_category.id,
      createObject: app.product_category.models.create,
      updateObject: app.product_category.models.update,
      onCreate: function() {
        app.router.navigate("list/product_category", {
          trigger: true
        });
      },
      onSave: function() {
        app.router.navigate("view/product_category/" + product_category.id, {
          trigger: true
        });
      },
      edit_route: "edit/product_category/",
    });

    return vm;

  }
  app.product_category.view_models.create = function() {
    var vm = buildProductCategoryVM({}, app.product_category.views.create);
    vm.title = "اضافة فئة انتاجية";
    vm.mode = "create";
    vm.setState();
  };

  app.product_category.view_models.edit = function(segment_id) {
    app.product_category.models.fetch(segment_id).done(function(segment) {
      var vm = buildProductCategoryVM(segment, app.product_category.views.edit);
      vm.title = "تعديل فئة انتاجية";
      vm.mode = "edit";
      vm.setState();
    });

  };
  app.product_category.view_models.view = function(segment_id) {
    app.product_category.models.fetch(segment_id).done(function(segment) {
      var vm = buildProductCategoryVM(segment, app.product_category.views.view);
      vm.title = "بيانات فئة انتاجية";
      vm.mode = "read";
      vm.setState();
    });

  };
  app.product_category.view_models.list = function() {
    var vm = utils.initializeListVM({
      getObjects: app.product_category.models.fetchList,
      render: app.product_category.views.list,
      title : "قائمة الفئات الانتاجية",
      columns: [{
        label: 'الاسم',
        name: "name",
        value: function(segment) {
          return segment.name;
        },
        orderable: true,
      }],
      order_column: "name",
      onSelectRow: function(segment_id) {
        app.router.navigate("view/product_category/" + segment_id, {
          trigger: true
        });
      }
    });

    vm.setState();
  };
};

var utils = require('../utils')();
module.exports = function(app) {

  app.product_category.views.create = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'product_category');
  };

  app.product_category.views.edit = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'product_category');
  };

  app.product_category.views.view = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'product_category');
  };

  app.product_category.views.list = function(vm, emitter) {
    utils.renderList(vm, emitter);

    $("#import").uploadFile({
      url: "/import/product-categories",
      multiple: false,
      dragDrop: false,
      maxFileCount: 1,
      fileName: "product_categories",
      uploadStr: "استيراد"
    });
  };


};

var utils = require('../utils')();

module.exports = function(app) {

  app.user.views.create = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'user');
  };
  app.user.views.edit = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'user');
  };

  app.user.views.view = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'user');
  };

  app.user.views.list = function(vm, emitter) {
    utils.renderList(vm, emitter);

    $("#import").uploadFile({
      url: "/import/users",
      multiple: false,
      dragDrop: false,
      maxFileCount: 1,
      fileName: "users",
      uploadStr: "استيراد"
    });
  };


};

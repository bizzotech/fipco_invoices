var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var validations = [{
    fn: function(user) {
      return !user.username;
    },
    msg: "من فضلك ادخل اسم المستخدم"
  }, {
    fn: function(user) {
      return !user.password;
    },
    msg: "من فضلك ادخل كلمة المرور"
  }];

  app.user.models.create = utils.create('/users', validations);
  app.user.models.update = utils.update('/users/', validations);
  app.user.models.fetch = utils.fetch('/users/', R.identity);
  app.user.models.fetchList = function(data) {
    return $.get('/users-table', data);
  };
};

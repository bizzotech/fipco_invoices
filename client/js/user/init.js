module.exports = function(app) {
  app.user = {};
  app.user.models = {};
  app.user.view_models = {};
  app.user.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

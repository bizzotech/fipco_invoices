var ee = require('event-emitter');
var utils = require('../utils')();
var R = require('ramda');
module.exports = function(app) {
  var roles = [{
    id: "admin",
    name: "مشرف"
  }, {
    id: "manager",
    name: "مدير"
  }, {
    id: "accountant",
    name: "محاسب"
  }, {
    id: "agent",
    name: "مندوب"
  }];

  function buildUserVM(user, render) {
    var fields = {};
    fields.name = {
      value: user.name || ""
    };
    fields.username = {
      value: user.username || ""
    };
    fields.phone = {
      value: user.phone || ""
    };
    fields.password = {
      type: "password",
      value: user.password || "",
      hidden: function() {
        return vm.mode !== "create";
      }
    };
    fields.role = {
      value: user.role || "",
      name: function() {
        var c = roles.find(function(role) {
          return role.id == fields.role.value;
        });
        return c ? c.name : "";
      },
      options: function() {
        return roles.filter(function(role) {
          return role.id != fields.role.value;
        });
      }
    };
    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      object_id: user.id,
      createObject: app.user.models.create,
      updateObject: app.user.models.update,
      onCreate: function() {
        app.router.navigate("list/user", {
          trigger: true
        });
      },
      onSave: function() {
        app.router.navigate("view/user/" + user.id, {
          trigger: true
        });
      },
      edit_route: "edit/user/",
    });

    return vm;
  }
  app.user.view_models.create = function() {
    var vm = buildUserVM({}, app.user.views.create);
    vm.title = "اضافة مستخدم";
    vm.mode = "create";
    vm.setState();
  };

  app.user.view_models.edit = function(user_id) {
    app.user.models.fetch(user_id).done(function(user) {
      var vm = buildUserVM(user, app.user.views.edit);
      vm.title = "تعديل بيانات مستخدم";
      vm.mode = "edit";
      vm.setState();
    });
  };

  app.user.view_models.view = function(user_id) {
    app.user.models.fetch(user_id).done(function(user) {
      var vm = buildUserVM(user, app.user.views.view);
      vm.title = "بيانات مستخدم";
      vm.mode = "read";
      vm.setState();
    });
  };

  app.user.view_models.list = function() {
    var vm = utils.initializeListVM({
      getObjects: app.user.models.fetchList,
      render: app.user.views.list,
      title : "قائمة المستخدمين",
      columns: [{
        label: 'اسم المستخدم',
        name: "name",
        value: function(user) {
          return user.name;
        },
        orderable: true,
      }, {
        label: 'الدور الوظيفي',
        name: "role",
        value: function(user) {
          return user.role;
        },
        orderable: true,
      }, {
        label: 'رقم الهاتف',
        name: "phone",
        value: function(user) {
          return user.phone;
        },
        orderable: true,
      }],
      order_column: "name",
      onSelectRow: function(user_id) {
        app.router.navigate("view/user/" + user_id, {
          trigger: true
        });
      }
    });

    vm.setState();
  };
};

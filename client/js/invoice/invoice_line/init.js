module.exports = function(app) {
  app.invoice_line = {};
  app.invoice_line.models = {};
  app.invoice_line.view_models = {};
  app.invoice_line.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

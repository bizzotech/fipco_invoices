var utils = require('../../utils')();

module.exports = function(app) {
  var validations = [];
  app.invoice_line.models.create = utils.create('/invoice-lines', validations);
  app.invoice_line.models.update = utils.update('/invoice-lines/', validations);
};

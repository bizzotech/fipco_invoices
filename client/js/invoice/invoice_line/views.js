var utils = require('../../utils')();

module.exports = function(app) {
  app.invoice_line.views.create = function(vm, emitter) {
    utils.renderFormInModal(vm, emitter, 'invoice_line');
  };

  app.invoice_line.views.edit = function(vm, emitter) {
    utils.renderFormInModal(vm, emitter, 'invoice_line');
  };

};

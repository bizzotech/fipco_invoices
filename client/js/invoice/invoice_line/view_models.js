var ee = require('event-emitter');
var utils = require('../../utils')();
var R = require('ramda');

var validations = [{
  fn: function(invoice_line) {
    return !invoice_line.product_id;
  },
  msg: "من فضلك اختر احد المنتجات"
}, {
  fn: function(invoice_line) {
    return !invoice_line.price;
  },
  msg: "من فضلك ادخل السعر"
}, {
  fn: function(invoice_line) {
    return !invoice_line.quantity;

  },
  msg: "من فضلك ادخل الكمية"
}];

module.exports = function(app) {
  function buildInvoiceLineVM(invoice_line, render, parent, index) {
    var fields = {};
    fields.price = {
      type: "number",
      value: invoice_line.price || ""
    };
    fields.id = {
      value: invoice_line.id || ""
    };
    fields.quantity = {
      type: "number",
      value: invoice_line.quantity || ""
    };
    fields.product_id = {
      value: invoice_line.product_id || "",
      name: function() {
        var cs = fields.product_id.options.find(function(segment) {
          return segment.id == fields.product_id.value;
        });
        return cs ? cs.name : (fields.product_id.default || "");
      },
      options: [],
      order_by: "name",
      query: function() {
        return fields.product_id.name();
      },
      fetchOptions: app.product.models.fetchList,
      focus: false
    };

    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      validations: validations,
      object_id: invoice_line.id,
    });
    return vm;
  }


  app.invoice_line.view_models.create = function(parent) {
    var vm = buildInvoiceLineVM({}, app.invoice_line.views.create, parent);
    vm.title = "اضافة سطر";
    vm.mode = "create";
    vm.create = function() {
      if (vm.validate()) {
        var line = R.map(function(field) {
          return field.value;
        }, vm.fields);
        line.product = vm.fields.product_id.options.find(function(product) {
          return product.id == vm.fields.product_id.value;
        });
        parent.emitter.emit('line_added', line);
      }
    };

    vm.setState();
  };

  app.invoice_line.view_models.edit = function(invoice_line, parent, index) {
    var vm = buildInvoiceLineVM(invoice_line, app.invoice_line.views.edit, parent, index);
    vm.title = "تعديل سطر";
    vm.mode = "edit";
    vm.fields.product_id.default = invoice_line.product.name;
    vm.save = function() {
      if (vm.validate()) {
        var line = R.map(function(field) {
          return field.value;
        }, vm.fields);
        line.product = vm.fields.product_id.options.find(function(product) {
          return product.id == vm.fields.product_id.value;
        });
        parent.emitter.emit('line_changed', index, line);
      }
    };


    vm.setState();
  };

};

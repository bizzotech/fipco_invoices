var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var validations = [{
    fn: function(invoice) {
      return !invoice.customer_id;
    },
    msg: "من فضلك ادخل ااسم العميل "
  }, {
    fn: function(invoice) {
      return !invoice.date;
    },
    msg: "من فضلك ادخل رقم التاريخ "
  }, {
    fn: function(invoice) {
      return !invoice.serial;
    },
    msg: "من فضلك ادخل مسلسل الفاتورة"
  }, {
    fn: function(invoice) {
      return !invoice.invoice_lines;
    },
    msg: "ﻻ يمكن ادخال فاتورة فارغة"
  }];

  app.invoice.models.create = utils.create('/invoices', validations);
  app.invoice.models.update = utils.update('/invoices/', validations);
  app.invoice.models.fetch = utils.fetch('/invoices/', function(invoice) {
    invoice.paid = 0;
    invoice.total = 0;
    _.each(invoice.invoice_lines, function(line) {
      invoice.total += line.sub_total;
    });
    _.each(invoice.payments, function(payment) {
      invoice.paid += payment.amount;
    });
    invoice.remaining = invoice.total - invoice.paid;


    invoice.commissions_details = Handlebars.templates.commissions_details({
      commissions_per_rule: invoice.commissions_per_rule
    });
    return invoice;
  });

  // models.fetchInvoices = utils.fetchList('/invoices', function(invoices) {
  //   invoices.forEach(function(invoice) {
  //     invoice.paid = 0;
  //     invoice.count = 0;
  //     _.each(invoice.payments, function(payment) {
  //       var notified = payment.to_notify.split('-');
  //       if (notified.indexOf(String(user.id)) != -1) {
  //         invoice.count += 1;
  //       }
  //       invoice.paid += payment.amount;
  //     });
  //     invoice.remaining = invoice.total - invoice.paid;
  //   });
  //   return invoices;
  // });
  app.invoice.models.fetchList = function(data) {
    return $.get('/invoices-table', data);
  };
};

var utils = require('../../utils')();

module.exports = function(app) {
  app.deposit.views.create = function(vm, emitter) {
    utils.renderFormInModal(vm, emitter, 'deposit');
  };
};

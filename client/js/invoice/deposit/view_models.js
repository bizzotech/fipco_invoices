var ee = require('event-emitter');
var utils = require('../../utils')();
var R = require('ramda');

module.exports = function(app) {
  function buildDepositVM(payment_id, render, parent) {
    var validations = [];
    var fields = {};
    fields.deposit_number = {
      value: ""
    };
    fields.notes = {
      value: ""
    };

    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      validations: validations,
      object_id: payment_id,
    });

    return vm;
  }


  app.deposit.view_models.create = function(payment_id, parent) {
    var vm = buildDepositVM(payment_id, app.deposit.views.create, parent);
    vm.title = "ايداع";
    vm.mode = "create";

    vm.create = function() {
      if (vm.validate()) {
        var deposit = R.map(function(field) {
          return field.value;
        }, vm.fields);
        deposit.state = "deposited";
        app.payment.models.update(deposit, payment_id).done(utils.hideModalAndRefresh).fail(function(msg) {
          vm.error = msg;
          vm.setState();
        });
      }
    };
    vm.setState();
  };
};

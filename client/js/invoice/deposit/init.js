module.exports = function(app) {
  app.deposit = {};
  app.deposit.models = {};
  app.deposit.view_models = {};
  app.deposit.views = {};
  require('./views')(app);
  require('./view_models')(app);
};

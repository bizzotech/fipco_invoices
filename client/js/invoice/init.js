module.exports = function(app) {
  app.invoice = {};
  app.invoice.models = {};
  app.invoice.view_models = {};
  app.invoice.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);

  require('./invoice_line/init')(app);
  require('./payment/init')(app);
  require('./deposit/init')(app);
  require('./send/init')(app);
};

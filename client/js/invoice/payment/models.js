var utils = require('../../utils')();

module.exports = function(app) {
  var validations = [];

  app.payment.models.create = utils.create('/payments', validations);
  app.payment.models.update = utils.update('/payments/', validations);

};

var utils = require('../../utils')();

module.exports = function(app) {
  app.payment.views.create = function(vm, emitter) {
    utils.renderFormInModal(vm, emitter, 'payment');
  };

  app.payment.views.edit = function(vm, emitter) {
    utils.renderFormInModal(vm, emitter, 'payment');
  };

};

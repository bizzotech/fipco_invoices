var ee = require('event-emitter');
var utils = require('../../utils')();
var R = require('ramda');

module.exports = function(app) {
  function buildPaymentVM(payment, render, parent, index) {

    var validations = [{
      fn: function(payment) {
        return !payment.amount;
      },
      msg: "من فضلك ادخل المبلغ"
    }, {
      fn: function(payment) {
        return isNaN(payment.amount);
      },
      msg: "من فضلك ادخل المبلغ بصيغة رقمية و ليست نصية "
    }, {
      fn: function(payment) {
        var total = R.sum(parent.fields.invoice_lines.value.map(function(line) {
          return line.price * line.quantity;
        }));
        var paid = R.sum(parent.fields.payments.value.map(function(payment) {
          return payment.amount;
        }));
        return (paid + Number(payment.amount)) > total;
      },
      msg: "المبلغ المدفوع اكبر من المتبقي علي العميل ادخل المبلغ بشكل صحيح"
    }, {
      fn: function(payment) {
        return (payment.cash_or_credit == "credit") && !payment.due_date;
      },
      msg: "من فضلك ادخل تاريخ الاستحقاق"
    }];

    var chash_or_credit_options = [{
      id: "cash",
      name: "نقدي"
    }, {
      id: "credit",
      name: "شيك"
    }];
    var states = [{
      id: "sent",
      name: "تم ارساله"
    }, {
      id: "deposited",
      name: "تم ايداعه"
    }];

    var fields = {};
    fields.id = {
      value: payment.id || ""
    };
    fields.amount = {
      type: "number",
      value: payment.amount || ""
    };
    fields.cash_or_credit = {
      value: payment.cash_or_credit || "",
      name: function() {
        var c = chash_or_credit_options.find(function(cash_or_credit) {
          return cash_or_credit.id == fields.cash_or_credit.value;
        });
        return c ? c.name : "";
      },
      options: function() {
        return chash_or_credit_options.filter(function(cash_or_credit) {
          return cash_or_credit.id != fields.cash_or_credit.value;
        });
      }
    };
    fields.to_notify = {
      value: payment.to_notify || ""
    };
    fields.due_date = {
      value: payment.due_date || "",
      type: "date",
      isDate: true,
      hidden: function() {
        return fields.cash_or_credit.value != "credit";
      }
    };
    fields.deposit_number = {
      value: payment.deposit_number || "",
      hidden: function() {
        return fields.state.value != "deposited";
      }
    };
    fields.notes = {
      value: payment.notes || ""
    };
    fields.state = {
      value: payment.state || "",
      name: function() {
        var c = states.find(function(state) {
          return state.id == fields.state.value;
        });
        return c ? c.name : "";
      },
      options: function() {
        return states.filter(function(state) {
          return state.id != fields.state.value;
        });
      },
      hidden: function() {
        return vm.mode == "create";
      }
    };
    fields.received = {
      value: payment.received || "0",
      checked: function() {
        return fields.received.value == "1" ? "checked" : "";
      },
      hidden: function() {
        return vm.mode == "create";
      }
    };
    fields.non_commissionable = {
      value: payment.non_commissionable || "0",
      checked: function() {
        return fields.non_commissionable.value == "1" ? "checked" : "";
      },
      hidden: function() {
        return vm.mode == "create";
      }
    };
    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      validations: validations,
      object_id: payment.id,
    });

    vm.isNew = function() {
      if (!vm.fields.state) {
        return true;
      }
    };
    vm.isSent = function() {
      if (vm.fields.state == "sent") {
        return true;
      }
    };
    vm.isNotReceived = function() {
      if (!vm.fields.received) {
        return true;
      }
    };

    return vm;
  }


  app.payment.view_models.create = function(parent) {
    var vm = buildPaymentVM({}, app.payment.views.create, parent);
    vm.title = "اضافة تحصيل";
    vm.mode = "create";

    vm.create = function() {
      if (vm.validate()) {
        var payment = R.map(function(field) {
          return field.value;
        }, vm.fields);
        app.payment.models.create({
          date: new Date(),
          amount: payment.amount,
          cash_or_credit: payment.cash_or_credit,
          invoice_id: parent.fields.id.value
        }).done(utils.hideModalAndRefresh).fail(function(msg) {
          vm.error = msg;
          vm.setState();
        });
      }
    };

    vm.setState();
  };

  app.payment.view_models.edit = function(payment, parent, index) {
    var vm = buildPaymentVM(payment, app.payment.views.edit, parent, index);
    vm.title = "تعديل تحصيل";
    vm.mode = "edit";

    vm.save = function() {
      if (vm.validate()) {
        parent.emitter.emit('payment_changed', index, R.map(function(field) {
          return field.value;
        }, vm.fields));
      }
    };

    vm.setState();
  };

};

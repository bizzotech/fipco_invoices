module.exports = function(app) {
  app.payment = {};
  app.payment.models = {};
  app.payment.view_models = {};
  app.payment.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

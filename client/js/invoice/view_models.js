var ee = require('event-emitter');
var utils = require('../utils')();
var R = require('ramda');
module.exports = function(app) {
  function buildInvoiceVM(invoice, render) {
    var fields = {};
    fields.id = {
      value: invoice.id || ""
    };
    fields.customer_id = {
      value: invoice.customer_id || "",
      name: function() {
        var cs = fields.customer_id.options.find(function(segment) {
          return segment.id == fields.customer_id.value;
        });
        return cs ? cs.name : (fields.customer_id.default || "");
      },
      options: [],
      order_by: "name",
      query: function() {
        return fields.customer_id.name();
      },
      fetchOptions: app.customer.models.fetchList,
      focus: false
    };
    fields.user_id = {
      value: invoice.user_id || "",
      name: function() {
        var cs = fields.user_id.options.find(function(segment) {
          return segment.id == fields.user_id.value;
        });
        return cs ? cs.name : (fields.user_id.default || "");
      },
      options: [],
      order_by: "name",
      query: function() {
        return fields.user_id.name();
      },
      fetchOptions: app.user.models.fetchList,
      focus: false
    };
    fields.serial = {
      value: invoice.serial || ""
    };
    fields.date = {
      value: invoice.date || new Date(),
      type: "date",
      isDate: true
    };
    fields.invoice_lines = {
      value: invoice.invoice_lines || [],
    };
    fields.payments = {
      value: invoice.payments || [],
    };
    fields.commissions_details = {
      value: invoice.commissions_details || "",
    };

    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      object_id: invoice.id,
      createObject: app.invoice.models.create,
      updateObject: app.invoice.models.update,
      onCreate: function() {
        app.router.navigate("list/invoice", {
          trigger: true
        });
      },
      onSave: function() {
        app.router.navigate("view/invoice/" + invoice.id, {
          trigger: true
        });
      },
      edit_route: "edit/invoice/",
    });

    vm.isPaid = function() {
      return invoice.remaining === 0;
    };

    var emitter = vm.emitter;
    emitter.on('line_added', function(line) {
      vm.fields.invoice_lines.value.push(line);
      vm.setState();
    });
    emitter.on('line_changed', function(index, line) {
      vm.fields.invoice_lines.value[index] = line;
      vm.fields.invoice_lines.value[index].edited = true;
      vm.setState();
    });

    vm.addLine = function() {
      app.invoice_line.view_models.create(vm);
    };
    vm.editLine = function(index) {
      app.invoice_line.view_models.edit(vm.fields.invoice_lines.value[index], vm, index);
    };

    emitter.on('payment_changed', function(index, payment) {
      vm.fields.payments.value[index] = payment;
      vm.fields.payments.value[index].edited = true;
      vm.setState();
    });

    vm.addPayment = function() {
      app.payment.view_models.create(vm);
    };
    vm.editPayment = function(index) {
      app.payment.view_models.edit(vm.fields.payments.value[index], vm, index);
    };

    vm.deposit = function(payment_id) {
      app.deposit.view_models.create(payment_id, vm);
    };

    vm.send = function(payment_id) {
      app.send.view_models.create(payment_id, vm);
    };

    //vm.setState();
    return vm;
  }


  app.invoice.view_models.create = function() {
    var vm = buildInvoiceVM({}, app.invoice.views.create);
    vm.title = "اضافة فاتورة";
    vm.mode = "create";
    vm.setState();
  };

  app.invoice.view_models.edit = function(invoice_id) {
    app.invoice.models.fetch(invoice_id).done(function(invoice) {
      var vm = buildInvoiceVM(invoice, app.invoice.views.edit);
      vm.title = "تعديل بيانات فاتورة";
      vm.mode = "edit";
      vm.fields.customer_id.default = invoice.customer.name;
      vm.fields.user_id.default = invoice.user.name;
      vm.setState();
    });
  };

  app.invoice.view_models.view = function(invoice_id) {
    app.invoice.models.fetch(invoice_id).done(function(invoice) {
      var vm = buildInvoiceVM(invoice, app.invoice.views.view);
      vm.title = "بيانات فاتورة";
      vm.mode = "read";
      vm.fields.customer_id.default = invoice.customer.name;
      vm.fields.user_id.default = invoice.user.name;
      vm.setState();
    });
  };

  app.invoice.view_models.list = function() {
    var vm = utils.initializeListVM({
      getObjects: app.invoice.models.fetchList,
      render: app.invoice.views.list,
      title : "قائمة الفواتير",
      columns: [{
        label: 'العميل',
        name: "customer.name",
        value: function(invoice) {
          return invoice.customer.name;
        },
        orderable: false,
      }, {
        label: 'رقم الفاتورة',
        name: "serial",
        value: function(invoice) {
          return invoice.serial;
        },
        orderable: true,
      }, {
        label: 'التاريخ',
        name: "date",
        value: function(invoice) {
          return moment(invoice.date).format("YYYY-MM-DD");
        },
        orderable: true
      }, {
        //   label: 'الاجمالي',
        //   name: "invoice_lines",
        //   value: function(invoice) {
        //     return R.sum(invoice.invoice_lines.map(function(line) {
        //       return line.price * line.quantity;
        //     }));
        //   }
        // }, {
        label: 'تاريخ اخر تعديل',
        name: "updatedAt",
        value: function(invoice) {
          return moment(invoice.updatedAt).format("YYYY-MM-DD");
        },
        orderable: true
      }],
      order_column: "updatedAt",
      onSelectRow: function(invoice_id) {
        router.navigate("view/invoice/" + invoice_id, {
          trigger: true
        });
      },
      // className: function(invoice) {
      //   var updated_invoice = false;
      //   invoice.payments.forEach(function(payment) {
      //     if (payment.to_notify.split('-').indexOf(String(app.current_user.id)) != -1) {
      //       updated_invoice = true;
      //     }
      //   });
      //   if (updated_invoice) {
      //     return " info";
      //   } else {
      //     return "";
      //   }
      // }
    });

    vm.setState();
  };


};

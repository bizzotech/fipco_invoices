var ee = require('event-emitter');
var utils = require('../../utils')();
var R = require('ramda');

module.exports = function(app) {
  function buildSendVM(payment_id, render, parent) {
    var validations = [];
    var fields = {};
    fields.notes = {
      value: ""
    };

    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      validations: validations,
      object_id: payment_id,
    });

    return vm;
  }


  app.send.view_models.create = function(payment_id, parent) {
    var vm = buildSendVM(payment_id, app.send.views.create, parent);
    vm.title = "ارسال";
    vm.mode = "create";

    vm.create = function() {
      if (vm.validate()) {
        var send = R.map(function(field) {
          return field.value;
        }, vm.fields);
        send.state = "sent";
        app.payment.models.update(send, payment_id).done(utils.hideModalAndRefresh).fail(function(msg) {
          vm.error = msg;
          vm.setState();
        });
      }
    };
    vm.setState();
  };
};

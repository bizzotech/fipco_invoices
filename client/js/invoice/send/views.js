var utils = require('../../utils')();

module.exports = function(app) {
  app.send.views.create = function(vm, emitter) {
    utils.renderFormInModal(vm, emitter, 'send');
  };
};

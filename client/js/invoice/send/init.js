module.exports = function(app) {
  app.send = {};
  app.send.models = {};
  app.send.view_models = {};
  app.send.views = {};
  require('./views')(app);
  require('./view_models')(app);
};

var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  app.invoice.views.create = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'invoice');

    $('#btn-add-line').click(function(e) {
      e.preventDefault();
      vm.addLine();
    });
    $('.btn-edit-line').click(function(e) {
      e.preventDefault();
      vm.editLine($(this).attr("data-index"));
    });
    emitter.on('line_added', function(line) {
      $('.modal').modal('hide');
    });
    emitter.on('line_changed', function(index, line) {
      $('.modal').modal('hide');
    });


  };

  app.invoice.views.view = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'invoice');

    $('#btn-add-payment').click(function(e) {
      e.preventDefault();
      vm.addPayment();
    });

    $('.btn-deposit').click(function(e) {
      e.preventDefault();
      var payment_id = $(this).attr("data-id");
      vm.deposit(payment_id);
    });

    $('.btn-send').click(function(e) {
      e.preventDefault();
      var payment_id = $(this).attr("data-id");
      vm.send(payment_id);
    });

    $('.btn-received').click(function(e) {
      e.preventDefault();
      var payment_id = $(this).attr("data-id");
      app.payment.models.update({
        received: true
      }, payment_id).done(utils.refresh).fail(utils.showMsgOnAlert);

    });
    $('[data-toggle="popover"]').popover();
    $.get('/seen/invoices/' + vm.fields.id.value);
  };

  app.invoice.views.edit = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'invoice');

    $('#btn-add-line').click(function(e) {
      e.preventDefault();
      vm.addLine();
    });
    $('.btn-edit-line').click(function(e) {
      e.preventDefault();
      vm.editLine($(this).attr("data-index"));
    });
    $('.btn-edit-payment').click(function(e) {
      e.preventDefault();
      vm.editPayment($(this).attr("data-index"));
    });

    emitter.on('line_added', function(line) {
      $('.modal').modal('hide');
    });
    emitter.on('line_changed', function(index, line) {
      $('.modal').modal('hide');
    });
    emitter.on('payment_changed', function(index, payment) {
      $('.modal').modal('hide');
    });

  };
  app.invoice.views.list = function(vm, emitter) {
    // var template = Handlebars.templates['invoice_list'];
    // utils.render(template, {});
    //
    // $(document).ready(function() {
    //   var table = $('#all').DataTable({
    //     "language": {
    //       "url": "/lib/translation/dataTables.arabic.lang"
    //     },
    //     select: true,
    //     "serverSide": true,
    //     "ajax": "/invoices-table",
    //     "order": [[ 4, "desc" ]],
    //     "columns": [{
    //       "data": {
    //         _: 'customer.name',
    //         sort: 'customer.name'
    //       },
    //       orderable: false,
    //       "defaultContent": ""
    //     }, {
    //       "data": "serial",
    //       "defaultContent": ""
    //     }, {
    //       "data": "date",
    //       "render": function(data) {
    //         return moment(data).format("YYYY-MM-DD");
    //       },
    //       "defaultContent": ""
    //     }, {
    //       data: 'invoice_lines',
    //       render: function(lines) {
    //         return R.sum(R.pluck('sub_total', lines));
    //       },
    //       orderable: false,
    //       defaultContent: 0
    //     }, {
    //       "data": "updatedAt",
    //       "render": function(data) {
    //         return moment(data).format("YYYY-MM-DD");
    //       },
    //       "defaultContent": ""
    //     }],
    //     fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
    //       // Bold the grade for all 'A' grade browsers
    //       console.log(nRow);
    //       var invoice = aData;
    //       var updated_invoice = false;
    //       invoice.payments.forEach(function(payment) {
    //         if (payment.to_notify.split('-').indexOf(String(current_user.id)) != -1) {
    //           updated_invoice = true;
    //         }
    //       });
    //       if (updated_invoice) {
    //         nRow.className += " info";
    //       }
    //     }
    //   });
    //
    //   table.on('select', function(e, dt, type, indexes) {
    //     if (type === 'row') {
    //       var invoice_id = table.rows(indexes).data().pluck('id')[0];
    //       router.navigate("invoices/" + invoice_id, {
    //         trigger: true
    //       });
    //
    //     }
    //   });
    // });
    //

    utils.renderList(vm, emitter);

    $("#import").uploadFile({
      url: "/import/invoices",
      multiple: false,
      dragDrop: false,
      maxFileCount: 1,
      fileName: "invoices",
      uploadStr: "استيراد"
    });
  };
};

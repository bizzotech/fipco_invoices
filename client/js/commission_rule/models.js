var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var validations = [{
    fn: function(commission_rule) {
      return commission_rule.latency_from === "";
    },
    msg: "من فضلك ادخل ادني مدة "
  }, {
    fn: function(commission_rule) {
      return commission_rule.latency_from % 30 !== 0;
    },
    msg: "من فضلك ادخل مدة تقبل القسمة علي ٣٠  "
  }, {
    fn: function(commission_rule) {
      return isNaN(commission_rule.latency_from);
    },
    msg: "من فضلك ادخل المدة في شكل رقم  "
  }, {
    fn: function(commission_rule) {
      return commission_rule.latency_to === "";
    },
    msg: "من فضلك ادخل اقصي مدة "
  }, {
    fn: function(commission_rule) {
      return Number(commission_rule.latency_to) <= Number(commission_rule.latency_from);
    },
    msg: "من فضلك يجب ادخال رقم اكبر من ادني مدة في خانة اقصي مدة  "
  }, {
    fn: function(commission_rule) {
      return commission_rule.latency_to % 30 !== 0;
    },
    msg: "من فضلك ادخل مدة تقبل القسمة علي ٣٠  "
  }, {
    fn: function(commission_rule) {
      return isNaN(commission_rule.latency_to);
    },
    msg: "من فضلك ادخل المدة في شكل رقم  "
  }, {
    fn: function(commission_rule) {
      return isNaN(commission_rule.commission_base);
    },
    msg: "من فضلك ادخل وعاء العمولة في صورة رقمية    "
  }, {
    fn: function(commission_rule) {
      return commission_rule.commission_base > 1 || commission_rule.commission_base < 0;

    },
    msg: "من فضلك ادخل  وعاء العمولة  رقم يتراوح بين 0 - 1"
  }, {
    fn: function(commission_rule) {
      return !commission_rule.product_category_id;

    },
    msg: "من فضلك قم باختيار الفئة الانتاجية"
  }, {
    fn: function(commission_rule) {
      return !commission_rule.customer_segment_id;

    },
    msg: "من فضلك قم باختيار القطاع"
  }, {
    fn: function(commission_rule) {
      return isNaN(commission_rule.commission_value);
    },
    msg: "من فضلك ادخل قيمة الوعاء في صورة رقمية    "
  }, {
    fn: function(commission_rule) {
      if (commission_rule.percentage_or_amount == "percentage") {
        return commission_rule.commission_value > 1 || commission_rule.commission_value < 0;
      }
    },
    msg: "من فضلك ادخل قيمة العمولة رقم يتراوح بين 0 - 1"
  }, {
    fn: function(commission_rule) {
      return !commission_rule.percentage_or_amount;

    },
    msg: "من فضلك قم باختيار نسبة او مبلغ"
  }];
  app.commission_rule.models.create = utils.create('/commission-rules', validations);
  app.commission_rule.models.update = utils.update('/commission-rules/', validations);
  app.commission_rule.models.fetch = utils.fetch('/commission-rules/', R.identity);
  app.commission_rule.models.fetchList = function(data) {
    return $.get('/commission-rules-table', data);
  };
};

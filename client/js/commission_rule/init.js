module.exports = function(app) {
  app.commission_rule = {};
  app.commission_rule.models = {};
  app.commission_rule.view_models = {};
  app.commission_rule.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

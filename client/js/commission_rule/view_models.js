var ee = require('event-emitter');
var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var percentage_or_amounts = [{
    id: "percentage",
    name: "النسبة "
  }, {
    id: "amount",
    name: "مبلغ ثابت"
  }];

  function buildCommissionRuleVM(commission_rule, render) {
    var fields = {};
    fields.commission_value = {
      value: commission_rule.commission_value || ""
    };
    fields.latency_to = {
      type: "number",
      value: commission_rule.latency_to || ""
    };
    fields.latency_from = {
      type: "number",
      value: commission_rule.latency_from === undefined ? "" : commission_rule.latency_from
    };
    fields.customer_segment_id = {
      value: commission_rule.customer_segment_id || "",
      name: function() {
        var cs = fields.customer_segment_id.options.find(function(segment) {
          return segment.id == fields.customer_segment_id.value;
        });
        return cs ? cs.name : (fields.customer_segment_id.default || "");
      },
      options: [],
      order_by: "name",
      query: function() {
        return fields.customer_segment_id.name();
      },
      fetchOptions: app.customer_segment.models.fetchList,
      focus: false
    };

    fields.product_category_id = {
      value: commission_rule.product_category_id || "",
      name: function() {
        var pc = fields.product_category_id.options.find(function(category) {
          return category.id == fields.product_category_id.value;
        });
        return pc ? pc.name : (fields.product_category_id.default || "");
      },
      options: [],
      order_by: "name",
      query: function() {
        return fields.product_category_id.name();
      },
      fetchOptions: app.product_category.models.fetchList,
      focus: false
    };

    fields.percentage_or_amount = {
      value: commission_rule.percentage_or_amount || "",
      name: function() {
        var c = percentage_or_amounts.find(function(p_or_a) {
          return p_or_a.id == fields.percentage_or_amount.value;
        });
        return c ? c.name : "";
      },
      options: function() {
        return percentage_or_amounts.filter(function(p_or_a) {
          return p_or_a.id != fields.percentage_or_amount.value;
        });
      }
    };
    fields.commission_base = {
      value: commission_rule.commission_base || ""
    };
    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      object_id: commission_rule.id,
      createObject: app.commission_rule.models.create,
      updateObject: app.commission_rule.models.update,
      onCreate: function() {
        app.router.navigate("list/commission_rule", {
          trigger: true
        });
      },
      onSave: function() {
        app.router.navigate("view/commission_rule/" + commission_rule.id, {
          trigger: true
        });
      },
      edit_route: "edit/commission_rule/",
    });

    return vm;

  }

  app.commission_rule.view_models.create = function() {
    var vm = buildCommissionRuleVM({}, app.commission_rule.views.create);
    vm.title = "اضافة قاعدة احتساب عمولة";
    vm.mode = "create";
    vm.setState();
  };

  app.commission_rule.view_models.edit = function(commission_rule_id) {
    app.commission_rule.models.fetch(commission_rule_id).done(function(commission_rule) {
      var vm = buildCommissionRuleVM(commission_rule, app.commission_rule.views.edit);
      vm.title = "تعديل قاعدة احتساب عمولة";
      vm.mode = "edit";
      vm.fields.customer_segment_id.default = commission_rule.customer_segment.name;
      vm.fields.product_category_id.default = commission_rule.product_category.name;
      vm.setState();
    });
  };

  app.commission_rule.view_models.view = function(commission_rule_id) {
    app.commission_rule.models.fetch(commission_rule_id).done(function(commission_rule) {
      var vm = buildCommissionRuleVM(commission_rule, app.commission_rule.views.view);
      vm.title = "تفاصيل قاعدة احتساب عمولة";
      vm.mode = "read";
      vm.fields.customer_segment_id.default = commission_rule.customer_segment.name;
      vm.fields.product_category_id.default = commission_rule.product_category.name;
      vm.setState();
    });
  };

  app.commission_rule.view_models.list = function() {
    var vm = utils.initializeListVM({
      getObjects: app.commission_rule.models.fetchList,
      render: app.commission_rule.views.list,
      title : "قائمة قواعد احتساب العمولة",
      columns: [{
        label: 'من',
        name: "latency_from",
        value: function(commission_rule) {
          return commission_rule.latency_from;
        },
        orderable: true,
      },{
        label: 'الي',
        name: "latency_to",
        value: function(commission_rule) {
          return commission_rule.latency_to;
        },
        orderable: true,
      }, {
        label: 'وعاء العمولة',
        name: "commission_base",
        value: function(commission_rule) {
          return commission_rule.commission_base;
        },
        orderable: true,
      }, {
        label: 'قطاع العميل',
        name: "customer_segment.name",
        value: function(customer) {
          return customer.customer_segment.name;
        }
      }, {
        label: 'فئة انتاجية',
        name: "product_category.name",
        value: function(customer) {
          return customer.product_category.name;
        }
      }, {
        label: 'قيمة العمولة',
        name: "commission_value",
        value: function(commission_rule) {
          return commission_rule.commission_value;
        },
        orderable: true,
      }, {
        label: 'نسبة او مبلغ',
        name: "percentage_or_amount",
        value: function(commission_rule) {
          return commission_rule.percentage_or_amount;
        },
        orderable: true,
      }],
      order_column: "id",
      onSelectRow: function(commission_rule_id) {
        app.router.navigate("view/commission_rule/" + commission_rule_id, {
          trigger: true
        });
      }
    });

    vm.setState();
  };

};

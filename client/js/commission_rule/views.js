var utils = require('../utils')();

module.exports = function(app) {

  app.commission_rule.views.create = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'commission_rule');
  };
  app.commission_rule.views.edit = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'commission_rule');
  };

  app.commission_rule.views.view = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'commission_rule');
  };

  app.commission_rule.views.list = function(vm, emitter) {
    utils.renderList(vm, emitter);

    $("#import").uploadFile({
      url: "/import/commission_rules",
      multiple: false,
      dragDrop: false,
      maxFileCount: 1,
      fileName: "commission_rules",
      uploadStr: "استيراد"
    });
  };


};

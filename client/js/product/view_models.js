var ee = require('event-emitter');
var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  function buildProductVM(product, render) {
    var fields = {};
    fields.name = {
      value: product.name || ""
    };
    fields.code = {
      value: product.code || ""
    };
    fields.product_category_id = {
      value: product.product_category_id || "",
      name: function() {
        var pc = fields.product_category_id.options.find(function(category) {
          return category.id == fields.product_category_id.value;
        });
        return pc ? pc.name : (fields.product_category_id.default || "");
      },
      options: [],
      order_by: "name",
      query: function() {
        return fields.product_category_id.name();
      },
      fetchOptions: app.product_category.models.fetchList,
      focus: false
    };
    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      object_id: product.id,
      createObject: app.product.models.create,
      updateObject: app.product.models.update,
      onCreate: function() {
        app.router.navigate("list/product", {
          trigger: true
        });
      },
      onSave: function() {
        app.router.navigate("view/product/" + product.id, {
          trigger: true
        });
      },
      edit_route: "edit/product/",
    });

    return vm;
  }
  app.product.view_models.create = function() {
    var vm = buildProductVM({}, app.product.views.create);
    vm.title = "اضافة منتج";
    vm.mode = "create";
    vm.setState();
  };

  app.product.view_models.edit = function(product_id) {
    app.product.models.fetch(product_id).done(function(product) {
      var vm = buildProductVM(product, app.product.views.edit);
      vm.title = "تعديل بيانات منتج";
      vm.mode = "edit";
      vm.fields.product_category_id.default = product.product_category.name;
      vm.setState();
    });
  };

  app.product.view_models.view = function(product_id) {
    app.product.models.fetch(product_id).done(function(product) {
      var vm = buildProductVM(product, app.product.views.view);
      vm.title = "بيانات منتج";
      vm.mode = "read";
      vm.fields.product_category_id.default = product.product_category.name;
      vm.setState();
    });
  };

  app.product.view_models.list = function(render) {
    var vm = utils.initializeListVM({
      getObjects: app.product.models.fetchList,
      render: app.product.views.list,
      title : "قائمة المنتجات",
      columns: [{
        label: 'اسم المنتج',
        name: "name",
        value: function(product) {
          return product.name;
        },
        orderable : true
      }, {
        label: 'كود المنتج',
        name: "code",
        value: function(product) {
          return product.code;
        },
        orderable : true
      }, {
        label: 'الفئة الانتاجية',
        name: "product_category.name",
        value: function(product) {
          return product.product_category.name;
        }
      }],
      onSelectRow : function(product_id){
        app.router.navigate("view/product/" + product_id, {
          trigger: true
        });
      }
    });

    vm.setState();
  };

};

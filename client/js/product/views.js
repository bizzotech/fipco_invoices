var utils = require('../utils')();

module.exports = function(app) {

  app.product.views.create = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'product');
  };
  app.product.views.edit = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'product');
  };

  app.product.views.view = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'product');
  };

  app.product.views.list = function(vm, emitter) {
    utils.renderList(vm, emitter);

    $("#import").uploadFile({
      url: "/import/products",
      multiple: false,
      dragDrop: false,
      maxFileCount: 1,
      fileName: "products",
      uploadStr: "استيراد"
    });
  };


};

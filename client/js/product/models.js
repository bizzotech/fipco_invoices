var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var validations = [{
    fn: function(product) {
      return !product.name;
    },
    msg: "من فضلك ادخل اسم المنتج"
  }, {
    fn: function(product) {
      return !product.code;
    },
    msg: "من فضلك ادخل كود المنتج"
  }, {
    fn: function(product) {
      return !product.product_category_id;

    },
    msg: "من فضلك قم باختيار الفئة الانتاجية"
  }];

  app.product.models.create = utils.create('/products', validations);
  app.product.models.update = utils.update('/products/', validations);
  app.product.models.fetch = utils.fetch('/products/', R.identity);
  //models.fetchProducts = utils.fetchList('/products', R.identity);
  app.product.models.fetchList = function(data) {
    return $.get('/products-table', data);
  };
};

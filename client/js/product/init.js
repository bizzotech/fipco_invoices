module.exports = function(app) {
  app.product = {};
  app.product.models = {};
  app.product.view_models = {};
  app.product.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

require('./dependencies');

//require('../lib/js/jquery.cookie')($);
//require('../lib/js/dataTables.select.min');
//require('../lib/js/jquery.noty');
//require('../lib/js/jquery.autogrow-textarea');
//require('../lib/js/jquery.uploadfile.min');
// require('../lib/js/charisma');

var app = {};
app.models = {};
app.views = {};
app.view_models = {};



var opts = {
  lines: 13, // The number of lines to draw
  length: 28, // The length of each line
  width: 14, // The line thickness
  radius: 42, // The radius of the inner circle
  scale: 1, // Scales overall size of the spinner
  corners: 1, // Corner roundness (0..1)
  color: '#000', // #rgb or #rrggbb or array of colors
  opacity: 0.25, // Opacity of the lines
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  speed: 1, // Rounds per second
  trail: 60, // Afterglow percentage
  fps: 20, // Frames per second when using setTimeout() as a fallback for CSS
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  className: 'spinner', // The CSS class to assign to the spinner
  top: '50%', // Top position relative to parent
  left: '50%', // Left position relative to parent
  shadow: true, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  position: 'absolute', // Element positioning
};
var target = document.querySelector('body');
var spinner = new Spinner(opts);

window.startSpinner = function() {
  $('button').attr('disabled', true);
  spinner.spin(target);
};
window.stopSpinner = function() {
  spinner.stop();
  $('button').attr('disabled', false);
};


Handlebars.registerPartial("input", Handlebars.templates.input);
Handlebars.registerPartial("select", Handlebars.templates.select);
Handlebars.registerPartial("checkbox", Handlebars.templates.checkbox);

Handlebars.registerPartial("select2", Handlebars.templates.select2);
Handlebars.registerHelper("callWith", function(fn, param) {
  return fn(param);
});

Handlebars.registerHelper("formatDate", function(date) {
  if (date) {
    return moment(date).format("YYYY-MM-DD");
  }
  return date;
});
Handlebars.registerHelper("userRole", function(role) {
  if (role == 'admin') {
    return "مشرف";
  }
  if (role == 'manager') {
    return "مدير";
  }
  if (role == 'accountant') {
    return "محاسب";
  }
  if (role == 'agent') {
    return "مندوب";
  }
});

Handlebars.registerHelper("percentageOrAmount", function(percentage_or_amount) {
  if (percentage_or_amount == 'percentage') {
    return "نسبة";
  }
  if (percentage_or_amount == 'amount') {
    return "مبلغ ثابت";
  }
  return percentage_or_amount;
});

Handlebars.registerHelper("ifNew", function(payment_state, block) {
  if (!payment_state || payment_state == "new") {
    return block.fn(this);
  }
});

Handlebars.registerHelper("ifDeposited", function(payment_state, block) {
  if (payment_state == "deposited") {
    return block.fn(this);
  }
});

Handlebars.registerHelper("ifSent", function(payment_state, block) {
  if (payment_state == "sent") {
    return block.fn(this);
  }
});

Handlebars.registerHelper("ifReceived", function(payment_received, block) {
  if (Number(payment_received)) {
    return block.fn(this);
  } else {
    return block.inverse(this);
  }
});

$.get('/invoice/notify').done(function(invoices_ids) {
  Handlebars.registerHelper("editedInvoices", function(invoices, block) {
    return invoices.filter(function(invoice) {
        return invoices_ids.indexOf(invoice.id) != -1;
      })
      .map(block.fn).join("\n");
  });
});

Handlebars.registerHelper("cashOrCredit", function(cash_or_credit) {
  if (cash_or_credit == "cash") {
    return "نقدي";
  }
  return "شيك";
});
Handlebars.registerHelper("paidInvoices", function(invoices, block) {

  return invoices.filter(function(invoice) {
    return invoice.remaining === 0;
  }).map(block.fn).join("\n");

});

$.get('/user/current').done(function(user) {
  app.current_user = user;
  Handlebars.registerHelper("paymentRowClass", function(received, to_notify) {

    var to_be_notified = to_notify.split('-');
    if (to_be_notified.indexOf(String(user.id)) != -1) {
      return "info";
    }
    if (user.role == "agent" && received == "received") {
      return "success";
    }
    return "";
  });


  Handlebars.registerHelper("isAgent", function(block) {
    if (user.role == 'agent') {
      return block.fn(this);
    }
  });
  Handlebars.registerHelper("isManager", function(block) {
    if (user.role == 'manager') {
      return block.fn(this);
    }
  });
  Handlebars.registerHelper("limitAccessTo", function(roles, block) {
    if (roles.split(',').indexOf(user.role) != -1) {
      return block.fn(this);
    }
  });
  Handlebars.registerHelper("forbidAccessTo", function(roles, block) {
    if (roles.split(',').indexOf(user.role) == -1) {
      return block.fn(this);
    }
  });



  var routes = require('./routes')(app, app.view_models, app.views, app.models, user);

  var Router = Backbone.Router.extend({
    routes: routes,
    execute: function(callback, args, name) {
      $('#app').empty();
      startSpinner();
      //args.push(parseQueryString(args.pop()));
      if (callback) callback.apply(this, args);
    }
  });
  window.router = new Router();


  //require('./models')(app.models, user);

  require('./views')(app.views, app.models, router, user);


  app.router = router;
  app.user = user;

  require('./user/init')(app);
  require('./customer/init')(app);
  require('./customer_segment/init')(app);
  require('./product/init')(app);
  require('./product_category/init')(app);
  require('./commission_rule/init')(app);
  require('./invoice/init')(app);

  Backbone.history.start();
  // Backbone.history.on('route', function(router, route, params) {
  //   startSpinner();
  // });

}).fail(function(err) {
  console.log(err);
});


// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function(event) {
  var doPrevent = false;
  if (event.keyCode === 8) {
    var d = event.srcElement || event.target;
    if ((d.tagName.toUpperCase() === 'INPUT' &&
        (
          d.type.toUpperCase() === 'TEXT' ||
          d.type.toUpperCase() === 'PASSWORD' ||
          d.type.toUpperCase() === 'FILE' ||
          d.type.toUpperCase() === 'SEARCH' ||
          d.type.toUpperCase() === 'EMAIL' ||
          d.type.toUpperCase() === 'NUMBER' ||
          d.type.toUpperCase() === 'DATE')
      ) ||
      d.tagName.toUpperCase() === 'TEXTAREA') {
      doPrevent = d.readOnly || d.disabled || d.parentElement.classList.contains("selectize-input") && !d.value;
    } else {
      doPrevent = true;
    }
  }

  if (doPrevent) {
    event.preventDefault();
  }
});

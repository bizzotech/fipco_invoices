var utils = require('../utils')();

module.exports = function(app) {

  app.customer.views.create = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'customer');
  };
  app.customer.views.edit = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'customer');
  };

  app.customer.views.view = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'customer');
  };

  app.customer.views.list = function(vm, emitter) {
    utils.renderList(vm, emitter);

    $("#import").uploadFile({
      url: "/import/customers",
      multiple: false,
      dragDrop: false,
      maxFileCount: 1,
      fileName: "customers",
      uploadStr: "استيراد"
    });
  };


};

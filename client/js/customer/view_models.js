var ee = require('event-emitter');
var utils = require('../utils')();
var R = require('ramda');

var validations = [{
  fn: function(customer) {
    return !customer.name;
  },
  msg: "من فضلك ادخل الاسم "
}, {
  fn: function(customer) {
    return !customer.code;
  },
  msg: "من فضلك ادخل كود العميل"
}, {
  fn: function(customer) {
    return !customer.customer_segment_id;

  },
  msg: "من فضلك قم باختيار القطاع"
}];

module.exports = function(app) {
  function buildCustomerVM(customer, render) {
    var fields = {};
    fields.name = {
      value: customer.name || ""
    };
    fields.code = {
      value: customer.code || ""
    };
    fields.area = {
      value: customer.area || ""
    };
    fields.customer_segment_id = {
      value: customer.customer_segment_id || "",
      name: function() {
        var cs = fields.customer_segment_id.options.find(function(segment) {
          return segment.id == fields.customer_segment_id.value;
        });
        return cs ? cs.name : (fields.customer_segment_id.default || "");
      },
      options: [],
      order_by: "name",
      query: function() {
        return fields.customer_segment_id.name();
      },
      fetchOptions: app.customer_segment.models.fetchList,
      focus: false
    };
    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      validations : validations,
      object_id: customer.id,
      createObject: app.customer.models.create,
      updateObject: app.customer.models.update,
      onCreate: function() {
        app.router.navigate("list/customer", {
          trigger: true
        });
      },
      onSave: function() {
        app.router.navigate("view/customer/" + customer.id, {
          trigger: true
        });
      },
      edit_route: "edit/customer/",
    });

    return vm;
  }


  app.customer.view_models.create = function() {
    var vm = buildCustomerVM({}, app.customer.views.create);
    vm.title = "اضافة عميل";
    vm.mode = "create";
    vm.setState();
  };

  app.customer.view_models.edit = function(customer_id) {
    app.customer.models.fetch(customer_id).done(function(customer) {
      var vm = buildCustomerVM(customer, app.customer.views.edit);
      vm.title = " تعديل بيانات عميل";
      vm.mode = "edit";
      vm.fields.customer_segment_id.default = customer.customer_segment.name;
      vm.setState();
    });
  };

  app.customer.view_models.view = function(customer_id) {
    app.customer.models.fetch(customer_id).done(function(customer) {
      var vm = buildCustomerVM(customer, app.customer.views.view);
      vm.title = "بيانات عميل";
      vm.mode = "read";
      vm.fields.customer_segment_id.default = customer.customer_segment.name;
      vm.setState();
    });
  };

  app.customer.view_models.list = function() {
    var vm = utils.initializeListVM({
      getObjects: app.customer.models.fetchList,
      render: app.customer.views.list,
      title : "قائمة العملاء",
      columns: [{
        label: 'اسم العميل',
        name: "name",
        value: function(customer) {
          return customer.name;
        },
        orderable: true,
      }, {
        label: 'كود العميل',
        name: "code",
        value: function(customer) {
          return customer.code;
        },
        orderable: true,
      }, {
        label: 'قطاع العميل',
        name: "customer_segment.name",
        value: function(customer) {
          return customer.customer_segment.name;
        }
      }],
      order_column: "name",
      onSelectRow: function(customer_id) {
        app.router.navigate("view/customer/" + customer_id, {
          trigger: true
        });
      }
    });

    vm.setState();
  };

};

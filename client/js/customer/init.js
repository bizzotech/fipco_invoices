module.exports = function(app) {
  app.customer = {};
  app.customer.models = {};
  app.customer.view_models = {};
  app.customer.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

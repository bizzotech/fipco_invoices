var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var validations = [];

  app.customer.models.create = utils.create('/customers', validations);
  app.customer.models.update = utils.update('/customers/', validations);
  app.customer.models.fetch = utils.fetch('/customers/', R.identity);
  //models.fetchCustomers = utils.fetchList('/customers', R.identity);

  app.customer.models.fetchList = function(data) {
    return $.get('/customers-table', data);
  };
};

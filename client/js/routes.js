module.exports = function(app, vms, views, models, user) {
  //require('./view_models/customer')(vms, models, views, user);
  //require('./view_models/invoice')(vms, models, views, user);
  //require('./view_models/invoice_line')(vms, models, views, user);
  //require('./view_models/payment')(vms, models, views, user);
  //require('./view_models/product')(vms, models, views, user);
  //require('./view_models/user')(vms, models, views, user);
  //require('./view_models/customer_segment')(vms, models, views, user);
  //require('./view_models/product_category')(vms, models, views, user);
  //require('./view_models/commission_rule')(vms, models, views, user);
  return {
    "": function() {
      views.renderDashboard();
    },
    "create/:model": function(model) {
      app[model].view_models.create();
    },
    "edit/:model/:object_id": function(model, object_id) {
      app[model].view_models.edit(object_id);
    },
    "view/:model/:object_id": function(model, object_id) {
      app[model].view_models.view(object_id);
    },
    "list/:model": function(model) {
      app[model].view_models.list();
    },
  };
};

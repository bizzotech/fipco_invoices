var ee = require('event-emitter');
var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  function buildCustomerSegmentVM(customer_segment, render) {
    var fields = {};
    fields.name = {
      value: customer_segment.name || ""
    };
    var vm = utils.initializeFormVM({
      fields: fields,
      render: render,
      object_id: customer_segment.id,
      createObject: app.customer_segment.models.create,
      updateObject: app.customer_segment.models.update,
      onCreate: function() {
        app.router.navigate("list/customer_segment", {
          trigger: true
        });
      },
      onSave: function() {
        app.router.navigate("view/customer_segment/" + customer_segment.id, {
          trigger: true
        });
      },
      edit_route: "edit/customer_segment/",
    });

    return vm;

  }
  app.customer_segment.view_models.create = function() {
    var vm = buildCustomerSegmentVM({}, app.customer_segment.views.create);
    vm.title = "اضافة قطاع";
    vm.mode = "create";
    vm.setState();
  };

  app.customer_segment.view_models.edit = function(segment_id) {
    app.customer_segment.models.fetch(segment_id).done(function(segment) {
      var vm = buildCustomerSegmentVM(segment, app.customer_segment.views.edit);
      vm.title = " تعديل بيانات القطاع";
      vm.mode = "edit";
      vm.setState();
    });

  };
  app.customer_segment.view_models.view = function(segment_id) {
    app.customer_segment.models.fetch(segment_id).done(function(segment) {
      var vm = buildCustomerSegmentVM(segment, app.customer_segment.views.view);
      vm.title = "بيانات القطاع";
      vm.mode = "read";
      vm.setState();
    });

  };
  app.customer_segment.view_models.list = function() {
    var vm = utils.initializeListVM({
      getObjects: app.customer_segment.models.fetchList,
      render: app.customer_segment.views.list,
      title : "قائمة القطاعات",
      columns: [{
        label: 'الاسم',
        name: "name",
        value: function(segment) {
          return segment.name;
        },
        orderable: true,
      }],
      order_column: "name",
      onSelectRow: function(segment_id) {
        router.navigate("view/customer_segment/" + segment_id, {
          trigger: true
        });
      }
    });

    vm.setState();
  };
};

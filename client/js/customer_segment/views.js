var utils = require('../utils')();
module.exports = function(app) {

  app.customer_segment.views.create = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'customer_segment');
  };

  app.customer_segment.views.edit = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'customer_segment');
  };

  app.customer_segment.views.view = function(vm, emitter) {
    utils.renderForm(vm, emitter, 'customer_segment');
  };

  app.customer_segment.views.list = function(vm, emitter) {
    utils.renderList(vm, emitter);

    $("#import").uploadFile({
      url: "/import/customer-segments",
      multiple: false,
      dragDrop: false,
      maxFileCount: 1,
      fileName: "customer_segments",
      uploadStr: "استيراد"
    });
  };
};

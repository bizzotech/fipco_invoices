var utils = require('../utils')();
var R = require('ramda');

module.exports = function(app) {
  var validations = [{
    fn: function(segment) {
      return !segment.name;
    },
    msg: "من فضلك ادخل اسم القطاع "
  }];

  app.customer_segment.models.create = utils.create('/customer-segments', validations);
  app.customer_segment.models.fetch = utils.fetch('/customer-segments/', R.identity);
  //models.fetchCustomerSegments = utils.fetchList('/customer-segments', R.identity);
  app.customer_segment.models.update = utils.update('/customer-segments/', validations);
  app.customer_segment.models.fetchList = function(data) {
    return $.get('/customer-segments-table', data);
  };
};

module.exports = function(app) {
  app.customer_segment = {};
  app.customer_segment.models = {};
  app.customer_segment.view_models = {};
  app.customer_segment.views = {};
  require('./models')(app);
  require('./views')(app);
  require('./view_models')(app);
};

var ee = require('event-emitter');
var R = require('ramda');
var diffDOM = require('./diffDOM');
var dd = new diffDOM();
module.exports = function() {
  var utils = {};
  require('./utils/model')(utils);
  require('./utils/ui')(utils);
  require('./utils/vm')(utils);
  return utils;
};

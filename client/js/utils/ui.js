var ee = require('event-emitter');
var R = require('ramda');
var diffDOM = require('../diffDOM');
var dd = new diffDOM();

module.exports = function(utils) {
  utils.refresh = function() {
    Backbone.history.loadUrl(Backbone.history.fragment);
  };

  utils.hideModalAndRefresh = function() {
    $('.modal').unbind();
    $('.modal').on('hidden.bs.modal', function(e) {
      utils.refresh();
    });
    $('.modal').modal('hide');
  };

  utils.showMsgOnAlert = function(msg) {
    $('.alert').removeClass('hide');
    $('.alert').text(msg);
  };

  utils.render = function(template, vm, container) {
    var selector = container || "#app";
    //$(selector).clone().appendTo('#temp');
    $(document.querySelector(selector).cloneNode(false)).appendTo('#temp');
    $('#temp ' + selector).html(template(utils.addCommonMethods(vm)));
    var c1 = document.querySelector(selector);
    var c2 = document.querySelector('#temp ' + selector);
    var diff = dd.diff(c1, c2);
    dd.apply(c1, diff);
    $(selector + ' *').unbind();
    $('#temp').html("");
    stopSpinner();
  };

  utils.renderList = function renderList(vm, emitter) {
    var template = Handlebars.templates.list_view;
    utils.render(template, vm);

    $('tr').click(function() {
      var row_id = $(this).attr('data-id');
      if (row_id) {
        emitter.emit('row_selected', row_id);
      }
    });

    $('.search').change(function(event) {
      var search_value = $(event.target).val();
      emitter.emit('search', search_value);
    });

    $('.length').change(function(event) {
      var length_value = $(event.target).val();
      emitter.emit('length', length_value);
    });

    $('.previous').click(function(event) {
      emitter.emit('previous');
    });
    $('.next').click(function(event) {
      emitter.emit('next');
    });
    $('.page-index').click(function(event) {
      var new_index = $(event.target).attr('data-index');
      emitter.emit('change_page', Number(new_index));
    });

    $('th[ordering]').click(function(event) {
      var col_index = $(event.target).attr('data-index');
      emitter.emit('change_order', Number(col_index));
    });

  };

  utils.renderForm = function renderForm(vm, emitter, template_name, container) {
    var template = Handlebars.templates[template_name];
    utils.render(template, vm, container);

    var namespace = container || "";

    $(namespace + ' form').change(function(event) {
      var obj = {};
      obj[event.target.id] = $(event.target).val();
      emitter.emit('changed', obj);
    });

    $(namespace + ' #btn-create').click(function(e) {
      e.preventDefault();
      vm.create();
    });
    $(namespace + ' #btn-save').click(function(e) {
      e.preventDefault();
      vm.save();
    });
    $(namespace + ' #btn-edit').click(function(e) {
      e.preventDefault();
      vm.edit();
    });

    $(namespace + ' .select-search').focus(function() {
      var field_name = $(this).attr('data-name');
      emitter.emit('select-focus', field_name);
      //$($(this).closest('.select')[0]).find('.select-elements').show();
    });
    $(namespace + ' .select-search').focusout(function() {
      var field_name = $(this).attr('data-name');
      setTimeout(function() {
        emitter.emit('select-unfocus', field_name);
      }, 100);
      //$($(this).closest('.select')[0]).find('.select-elements').hide();
    });

    $(namespace + ' .select-search').keyup(function(event) {
      var field_name = $(this).attr('data-name');
      var field_value = $(event.target).val();
      emitter.emit('select-query-change', field_name, field_value);
    });

    $(namespace + ' form').on('click', '.option', function() {
      var field_name = $(this).attr('data-name');
      var selected_option_id = $(this).attr('data-value');
      emitter.emit('change_selection', field_name, selected_option_id);
    });

  };

  utils.renderFormInModal = function renderFormInModal(vm, emitter, template_name) {
    utils.renderForm(vm, emitter, template_name, '#popup');
    $('#popup').on('hidden.bs.modal', function () {
      $('#popup').empty();
    });
  };

};

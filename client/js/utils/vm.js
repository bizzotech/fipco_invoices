var ee = require('event-emitter');
var R = require('ramda');
var diffDOM = require('../diffDOM');
var dd = new diffDOM();

module.exports = function(utils) {
  utils.addCommonMethods = function addCommonMethods(vm) {
    vm.isCreate = function() {
      return vm.mode == "create";
    };
    vm.isEdit = function() {
      return vm.mode == "edit";
    };
    vm.isRead = function() {
      return vm.mode == "read";
    };

    return vm;
  };

  utils.initializeListVM = function initializeListVM(config) {
    var emitter = ee({});
    var vm = {
      emitter: emitter,
      getObjects: config.getObjects,
      columns: config.columns,
      columns_names: function() {
        return R.map(R.pick(['name']), vm.columns);
      },
      title : config.title,
      search_value: "",
      page_value: 1,
      start_value: function() {
        return (vm.page_value - 1) * vm.length_value;
      },
      end_value: function() {
        return Number(vm.start_value() + vm.length_value);
      },
      length_value: 10,
      order_column: function() {
        return config.order_column || vm.columns_names()[0].name;
      },
      order_dir: config.order_dir || "asc",
      setState: function() {
        vm.getObjects({
          search: {
            value: vm.search_value
          },
          columns: vm.columns_names(),
          order: [{
            column: vm.order_column(),
            dir: vm.order_dir
          }],
          start: vm.start_value(),
          length: vm.length_value
        }).done(function(objects) {
          var pages_count;
          if (objects.recordsTotal % vm.length_value === 0) {
            pages_count = Math.floor(objects.recordsTotal / vm.length_value);
          } else {
            pages_count = Math.floor(objects.recordsTotal / vm.length_value) + 1;
          }

          config.render({
            objects: objects.data.map(function(o) {
              if (config.className) {
                o.className = config.className(o);
              }
              return o;
            }),
            columns: vm.columns.map(function(column) {
              column.ordering = function() {
                if (!column.orderable) {
                  return "none";
                }
                if (column.name != vm.order_column()) {
                  return "both";
                }
                if (vm.order_dir == "asc") {
                  return "desc";
                }
                return "asc";
              };
              return column;
            }),
            title : vm.title,
            search_value: vm.search_value,
            start_value: function() {
              return vm.start_value() + 1;
            },
            end_value: function() {
              if (vm.end_value() < objects.recordsTotal) {
                return vm.end_value();
              }
              return objects.recordsTotal;
            },
            total_value: objects.recordsTotal,
            length_options: function() {
              var selected = vm.length_value;
              var options = [10, 25, 50, 100];
              return options.map(function(option) {
                return {
                  value: option,
                  name: option,
                  is_selected: option == selected
                };
              });
            },
            pages: function() {
              if (pages_count < 8) {
                return R.range(1, pages_count + 1).map(function(p) {
                  return {
                    value: p,
                    current: p === vm.page_value,
                    disabled: false,
                  };
                });
              } else {
                var ps = [{
                  value: vm.page_value,
                  current: true,
                  disabled: false
                }];
                if (vm.page_value < 4) {
                  R.range(1, vm.page_value).reverse().forEach(function(p) {
                    ps.unshift({
                      value: p,
                      current: false,
                      disabled: false
                    });
                  });
                } else {
                  ps.unshift({
                    value: vm.page_value - 1,
                    current: false,
                    disabled: false
                  });
                  ps.unshift({
                    value: "...",
                    current: false,
                    disabled: true
                  });
                  ps.unshift({
                    value: 1,
                    current: false,
                    disabled: false
                  });
                }

                if (pages_count - vm.page_value < 4) {
                  R.range(vm.page_value + 1, pages_count + 1).forEach(function(p) {
                    ps.push({
                      value: p,
                      current: false,
                      disabled: false
                    });
                  });
                } else {
                  ps.push({
                    value: vm.page_value + 1,
                    current: false,
                    disabled: false
                  });
                  ps.push({
                    value: "...",
                    current: false,
                    disabled: true
                  });
                  ps.push({
                    value: pages_count,
                    current: false,
                    disabled: false
                  });
                }
                return ps;
              }

            },
            is_first_page: function() {
              return vm.page_value === 1;
            },
            is_last_page: function() {
              return vm.page_value === pages_count;
            }
          }, emitter);
        });
      }
    };

    emitter.on('row_selected', function(row_id) {
      if (config.onSelectRow) {
        config.onSelectRow(row_id);
      }
    });

    emitter.on('search', function(search_value) {
      vm.search_value = search_value;
      vm.setState();
    });

    emitter.on('length', function(length_value) {
      vm.length_value = length_value;
      vm.page_value = 1;
      vm.setState();
    });

    emitter.on('previous', function() {
      vm.page_value -= 1;
      vm.setState();
    });

    emitter.on('next', function() {
      vm.page_value += 1;
      vm.setState();
    });

    emitter.on('change_page', function(new_index) {
      if (vm.page_value != new_index) {
        vm.page_value = new_index;
        vm.setState();
      }
    });

    emitter.on('change_order', function(col_index) {
      var new_order_column = vm.columns[col_index].name;
      if (new_order_column == vm.order_column()) {
        vm.order_dir = vm.order_dir == "desc" ? "asc" : "desc";
      } else {
        vm.order_dir = "asc";
      }
      vm.order_column = function() {
        return new_order_column;
      };
      vm.setState();
    });

    return vm;
  };

  utils.initializeFormVM = function initializeFormVM(config) {
    var emitter = ee({});
    var vm = {
      emitter: emitter,
      fields: config.fields,
      validations: config.validations || [],
      setState: function() {
        config.render(vm, emitter);
      },
      validate: function() {
        var obj = R.map(function(field) {
          return field.value;
        }, vm.fields);
        for (var i = 0; i < vm.validations.length; i++) {
          if (vm.validations[i].fn(obj)) {
            vm.error = vm.validations[i].msg;
            vm.setState();
            return false;
          }
        }
        return true;
      },
      create: function() {
        if (vm.validate()) {
          return config.createObject(R.map(function(field) {
            return field.value;
          }, config.fields)).then(config.onCreate).fail(function(msg) {
            vm.error = msg;
            vm.setState();
          });
        }
      },
      save: function() {
        if (vm.validate()) {
          return config.updateObject(R.map(function(field) {
            return field.value;
          }, config.fields), config.object_id).then(config.onSave).fail(function(msg) {
            vm.error = msg;
            vm.setState();
          });
        }
      },
      edit: function() {
        router.navigate(config.edit_route + config.object_id, {
          trigger: true
        });
      }
    };

    emitter.on('changed', function(obj) {
      for (var p in obj) {
        if (vm.fields[p]) {
          vm.fields[p].value = obj[p];
        }
      }
      vm.setState();
    });

    emitter.on('select-focus', function(field_name) {
      vm.fields[field_name].focus = true;
      if (vm.fields[field_name].options.length > 0) {
        vm.setState();
      } else {
        var query = vm.fields[field_name].name();
        emitter.emit('select-query-change', field_name, query);
      }

    });
    emitter.on('select-unfocus', function(field_name) {
      vm.fields[field_name].focus = false;
      vm.setState();
    });
    emitter.on('select-query-change', function(field_name, field_value) {
      vm.fields[field_name].query = field_value;
      vm.fields[field_name].fetchOptions({
        search: {
          value: vm.fields[field_name].query
        },
        order: [{
          column: vm.fields[field_name].order_by || "id",
          dir: 'asc'
        }],
        start: 0,
        length: 10
      }).done(function(options) {
        vm.fields[field_name].options = options.data;
        vm.setState();
      });
    });
    emitter.on('change_selection', function(field_name, selected_option_id) {
      vm.fields[field_name].value = selected_option_id;
      vm.fields[field_name].query = vm.fields[field_name].name;
      vm.setState();
    });

    return vm;
  };

};

var ee = require('event-emitter');
var R = require('ramda');
var diffDOM = require('../diffDOM');
var dd = new diffDOM();

module.exports = function(utils) {
  utils.create = R.curry(function create(url, validations, obj) {
    var defer = $.Deferred();
    for (var i = 0; i < validations.length; i++) {
      if (validations[i].fn(obj)) {
        defer.reject(validations[i].msg);
        return defer;
      }
    }
    startSpinner();
    $.post(url, obj).done(function() {
      defer.resolve();
    }).fail(function(err) {
      if (err.status == 401) {
        return window.location.replace("/login");
      }
      if (err.responseJSON && err.responseJSON.errors && err.responseJSON.errors[0].type == "unique violation") {
        return defer.reject(err.responseJSON.errors[0].message);
      }
      defer.reject("Server Error");
    });
    return defer;
  });

  utils.update = R.curry(function update(url, validations, obj, obj_id) {
    var defer = $.Deferred();
    for (var i = 0; i < validations.length; i++) {
      if (validations[i].fn(obj)) {
        defer.reject(validations[i].msg);
        return defer;
      }
    }
    startSpinner();
    $.ajax({
      url: url + obj_id,
      type: 'PUT',
      data: obj
    }).done(function() {
      defer.resolve();
    }).fail(function(err) {
      if (err.status == 401) {
        return window.location.replace("/login");
      }
      defer.reject("Server Error");
    });
    return defer;
  });

  utils.fetch = R.curry(function fetch(url, transform, obj_id) {
    var defer = $.Deferred();
    $.get(url + obj_id).done(function(obj) {
      defer.resolve(transform(obj));
    }).fail(function(err) {
      if (err.status == 401) {
        return window.location.replace("/login");
      }
      defer.reject("Server Error");
    });
    return defer;
  });

  utils.fetchList = function(url, transform) {
    return function() {
      return utils.fetch(url, transform, "");
    };
  };

};

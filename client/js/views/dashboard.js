var utils = require('../utils')();
var R = require('ramda');

module.exports = function(views, models, router, user) {

  views.renderDashboard = function() {
    var template = Handlebars.templates['dashboard'];
    utils.render(template, {});
    // require('../../lib/flot-charts/excanvas');
    // require('../../lib/flot-charts/jquery.flot');
    // require('../../lib/flot-charts/jquery.flot.pie');
    // require('../../lib/flot-charts/jquery.flot.resize');
    // require('../init-chart');

    // $(document).ready(function() {
    //   var table = $('#agent-invoices').DataTable({
    //     "language": {
    //       "url": "/lib/translation/dataTables.arabic.lang"
    //     },
    //     select: true,
    //     "serverSide": true,
    //     "ajax": "/invoices-table",
    //     "fnServerParams": function(aoData) {
    //       aoData.agent_invoices = true;
    //     },
    //     "order": [
    //       [4, "desc"]
    //     ],
    //     "columns": [{
    //       "data": {
    //         _: 'customer.name',
    //         sort: 'customer.name'
    //       },
    //       orderable: false,
    //       "defaultContent": ""
    //     }, {
    //       "data": "serial",
    //       "defaultContent": ""
    //     }, {
    //       "data": "date",
    //       "render": function(data) {
    //         return moment(data).format("YYYY-MM-DD");
    //       },
    //       "defaultContent": ""
    //     }, {
    //       data: 'invoice_lines',
    //       render: function(lines) {
    //         return R.sum(R.pluck('sub_total', lines));
    //       },
    //       orderable: false,
    //       defaultContent: 0
    //     }, {
    //       "data": "updatedAt",
    //       "render": function(data) {
    //         return moment(data).format("YYYY-MM-DD");
    //       },
    //       "defaultContent": ""
    //     }]
    //   });
    //
    //   table.on('select', function(e, dt, type, indexes) {
    //     if (type === 'row') {
    //       var invoice_id = table.rows(indexes).data().pluck('id')[0];
    //       router.navigate("invoices/" + invoice_id, {
    //         trigger: true
    //       });
    //
    //     }
    //   });
    //
    // });

  };

};

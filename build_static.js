/*jshint esversion: 6 */
var uglifycss = require('uglifycss');
var UglifyJS = require("uglify-js");
var fs = require('fs');

var meta = require('./meta');
var css_files = meta.css.map(f => 'public' + f);
var uglified_css = uglifycss.processFiles(
  css_files, {
    maxLineLen: 500,
    expandVars: true
  }
);
fs.writeFileSync('public/lib/css/app.min.css', uglified_css);


var js_files = meta.js.map(f => 'public' + f);
var uglified_js = UglifyJS.minify(js_files).code;
fs.writeFileSync('public/lib/js/app.min.js', uglified_js);

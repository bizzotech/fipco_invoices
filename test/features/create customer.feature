Feature:Create customers page

  Scenario: Visiting create customer page
    Given I am on the home page
    When I fill username field with admin
    And I fill password field with admin
    And I click submit button
    And I click "اضافة عميل" link
    Then I should see a title called "اضافة عميل"
    Then I should see four fields in the page
    Then I should see fields called "اسم العميل : كود العميل : المنطقة : القطاع :"
    Then I should see a button called "تأكيد"

    When I fill name field with ""
    And I fill code field with ""
    And I fill area field with ""
    And I fill customer_segment_id field with ""
    And I click btn-create button
    Then It should show error massage

    When I fill name field with any_name
    And I fill code field with ""
    And I fill area field with ""
    And I fill customer_segment_id field with ""
    And I click btn-create button
    Then It should show error massage

    When I fill name field with any_name
    And I fill code field with any_code
    And I fill area field with ""
    And I fill customer_segment_id field with ""
    And I click btn-create button
    Then It should show error massage

    When I fill name field with any_name
    And I fill code field with any_code
    And I fill area field with ""
    And I fill customer_segment_id field with any_segment
    And I click btn-create button


    When I fill name field with any_name
    And I fill code field with any_code
    And I fill area field with any_area
    And I fill customer_segment_id field with any_segment
    And I click btn-create button

var sharedSteps = module.exports = function() {
  this.World = require('../support/world');

  this.Given(/^I am on the ([^\s]*) page$/, function(route) {
    if (route == 'home') {
      return this.visit('/');
    }
    return this.visit('/' + route);
  });

  this.When(/^I fill ([^\s]*) field with ([^\s]*)$/, function(field_id, value) {
    return this.browser.fill('#' + field_id, value);
  });

  this.When(/^I click ([^\s]*) button$/, function(button_id) {
    return this.browser.pressButton('#' + button_id);
  });

  this.When(/^I click "([^"]*)" link$/, function(link_content, cb) {
    //return this.browser.pressButton('#'+button_id);
    //return this.browser.open('/#'+li_id);
    //return this.browser.assert.attribute('a', '/#'+li);
    //var selector = 'a[href="' + "/#" + link + '"]';
    //return this.browser.clickLink(selector);
    //return this.visit('/#' + link);
    var links = this.browser.querySelectorAll('a');
    var link;
    Array.prototype.forEach.call(links, function(element) {
      if (element.innerHTML == link_content) {
        console.log(element.getAttribute('href'));
        link = element.getAttribute('href');
      }
    });
    if(link){
      this.visit(link).then(function(){
        cb();
      });
    }else {
      cb(new Error("No link named " + link_content));
    }
  });

  this.Then(/^I should see "([^"]*)"$/, function(text, next) {
    this.browser.text('body').should.include.string(text);
    next();
  });

  this.Then(/^Title should be "([^"]*)"$/, function(text) {
    return this.browser.assert.text('title', text);
    //next();
  });

  this.Then(/^I should see a title called "([^"]*)"$/, function(title){
   console.log(this.browser.errors);
    return this.browser.assert.text('#page-title', title);
   });

  this.Then(/^I should see a title called "([^\s]*)" "([^"]*)"$/, function(ID, text) {
    console.log(this.browser.errors);
    return this.browser.assert.text(ID, text);
  });

  this.Then(/^I should see fields called "([^"]*)"$/, function(text) {
    console.log(this.browser.errors);
    return this.browser.assert.text('label', text);
  });

  this.Then(/^It should show error massage$/, function() {
    console.log(this.browser.errors);
    return this.browser.assert.element('div[text=error]');

  });

  this.Then(/^I should see four fields in the page$/, function() {
    console.log(this.browser.errors);
    return this.browser.assert.elements('label', {
      exactly: 4
    });
  });

  this.Then(/^I should see a button called "([^"]*)"$/, function(text) {
    console.log(this.browser.errors);
    return this.browser.assert.element('button[text=' + text + ']');

  });
  // browser.response.toString().should.include 'Logout'

  this.And = this.When;
};

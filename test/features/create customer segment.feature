Feature:Customers segment page

  Scenario: Visiting create customer segment page
    Given I am on the home page
    When I fill username field with admin
    And I fill password field with admin
    And I click submit button
    And I click "اضافة قطاع" link
    Then I should see a title called "اضافة قطاع"

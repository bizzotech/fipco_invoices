Feature: Create product page

  Scenario: Visiting create product page
    Given I am on the home page
    When I fill username field with admin
    And I fill password field with admin
    And I click submit button
    And I click "اضافة منتج" link
    Then I should see a title called "اضافة منتج"

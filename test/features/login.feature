Feature: Default admin login

  Scenario: Using default admin account to login
    Given I am on the login page
    When I fill username field with admin
    And I fill password field with admin
    And I click submit button
    Then Title should be "Fipco Invoices"

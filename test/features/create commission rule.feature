@dev
Feature:Create commission rule page

  Scenario: Visiting create commission rule page
    Given I am on the home page
    When I fill username field with admin
    And I fill password field with admin
    And I click submit button
    And I click "اضافة قاعدة احتساب عمولة" link
    Then I should see a title called "اضافة قاعدة احتساب عمولة"

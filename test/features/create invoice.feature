Feature:Invoices page

  Scenario: Visiting invoices page
    Given I am on the home page
    When I fill username field with admin
    And I fill password field with admin
    And I click submit button
    And I click "اضافة فاتورة" link
    Then I should see a title called "اضافة فاتورة"

Feature: Customers page

  Scenario: Visiting the customers page
    Given I am on the home page
    When I fill username field with admin
    And I fill password field with admin
    And I click submit button
    And I click "قائمة العملاء" link
    Then I should see a title called "قائمة العملاء"

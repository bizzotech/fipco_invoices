var Browser = require('zombie');
var HTML5 = require('html5');
var should = require('should');
var models = require('../../../server').models;
var user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20';

var World = module.exports = function() {
  this.models = models;
  this.browser = new Browser({
    userAgent: user_agent,
    runScripts: true,
    debug: true,
    waitDuration: 10 * 1000,
    htmlParser: HTML5
  });

  this.page = function(path) {
    return "http://localhost:" + "8081" + path;
  };

  this.visit = function(path) {
    return this.browser.visit(this.page(path));
  };
};

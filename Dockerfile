FROM bizzo/bizzo-deps

ENV NODE_PATH /usr/local/lib/node_modules/

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app

EXPOSE 8080

